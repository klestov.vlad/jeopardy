import styled, { keyframes } from "styled-components";
import { useUserGameControl } from "../../shared/hooks/useUserGameControl";
import { Button, Heading, Input, Textarea } from "@chakra-ui/react";

import { GamerLayout } from "../../layouts/gamer-layout";
import leavesImage from "./images/leaves.png";
import { UserSpinToWinScreen } from "../user-spin-to-win-screen/user-spin-to-win-screen";
import CountUp, { useCountUp } from "react-countup";
import { useEffect, useRef, useState } from "react";

const COUNT_TIME = 2;

export const GameScreen = () => {
  const {
    user,
    isMyTurn,
    otherUserTurn,
    isSuperGame,
    isWinnerIsMe,
    isSpinToWin,
    isAuctionBit,
    isDemoSpinAvailable,
    setActiveUser,
    setUserAnswer,
    setUserBet,
    setSpinTry,
  } = useUserGameControl();

  const [inMemoryCount, setInMemoryCount] = useState(0);

  const updateCount = (count: number) => {
    setInMemoryCount(count);
  };

  useEffect(() => {
    setTimeout(() => {
      updateCount(user?.count || 0);
    }, COUNT_TIME * 1000);
  }, [user?.count]);

  if (isSpinToWin && isWinnerIsMe) {
    return (
      <UserSpinToWinScreen
        isWinnerIsMe={isWinnerIsMe}
        setSpinTry={setSpinTry}
        isDemoSpinAvailable={isDemoSpinAvailable}
      />
    );
  }

  return isSuperGame || isAuctionBit ? (
    <GamerLayout>
      <BackgroundWrapper>
        <TopImage src={leavesImage} />
      </BackgroundWrapper>
      {isAuctionBit && (
        <ContentWrapper>
          <Heading textAlign={"center"} color={"white"} size={"3xl"}>
            Сделай ставку
            <br />
            (от 0 до {user?.count})
          </Heading>
          <Input
            type="number"
            marginTop={10}
            backgroundColor={"white"}
            height={62}
            fontSize={32}
            placeholder="Введи ставку"
            value={user?.auctionBid || ""}
            onChange={(e) => setUserBet(e.target.value)}
            min={0}
            max={user?.count}
          />
        </ContentWrapper>
      )}

      {isSuperGame && (
        <ContentWrapper>
          <Heading textAlign={"center"} color={"white"} size={"4xl"}>
            Ответ на вопрос
          </Heading>
          <Textarea
            marginTop={10}
            backgroundColor={"white"}
            height={62}
            fontSize={32}
            placeholder="Введи ответ"
            value={user?.auctionAnswer}
            onChange={(e) => setUserAnswer(e.target.value)}
          />
        </ContentWrapper>
      )}
    </GamerLayout>
  ) : (
    <button onClick={setActiveUser}>
      <TextWrapper $isMyTurn={isMyTurn} $otherUserTurn={otherUserTurn}>
        <BigText>
          <CountUp start={inMemoryCount} end={user?.count || 0} duration={4} />
        </BigText>
      </TextWrapper>
    </button>
  );
};

const changeColor = keyframes`
    0% {
      background-color: #e45ebf;
    }
    100% {
      background-color: #343dbe;
    }
`;

const opacity = keyframes`
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
`;

const BigText = styled.p`
  font-size: 20vh;
  user-select: none;
  color: #fbe296;

  animation: ${opacity} 3s ease-in-out;
`;

const TextWrapper = styled.div<{
  $isMyTurn?: boolean;
  $otherUserTurn?: boolean;
}>`
  background: ${({ $isMyTurn, $otherUserTurn }) =>
    $isMyTurn ? "green" : $otherUserTurn ? "#be343d" : "#343dbe"};
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  height: 100svh;
  width: 100vw;
  animation: ${changeColor} 3s ease-in-out;
`;

const ContentWrapper = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const BackgroundWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-color: #e45ebf;
  overflow: hidden;
`;

const TopImage = styled.img`
  position: absolute;
  top: -15%;
  left: 0;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  width: 100%;
  transform: scale(2);
`;

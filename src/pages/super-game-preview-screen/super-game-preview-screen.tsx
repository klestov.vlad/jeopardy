import styled, { keyframes } from "styled-components";
import { Heading, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useSharedGameScreen } from "../../shared/hooks/useSharedGameScreen";
import { ActiveGameScreens } from "../../shared/constants/routes";

export const SuperGamePreviewScreen = () => {
  const { currentScreen, superGameData } = useSharedGameScreen();
  const navigate = useNavigate();

  useEffect(() => {
    if (!currentScreen) {
      return;
    }

    if (currentScreen !== ActiveGameScreens.SHARED_GAME_SCREEN) {
      navigate(currentScreen);
    }
  }, [currentScreen, navigate]);

  return (
    <Wrapper>
      <ShiftHeading
        textAlign={"center"}
        alignSelf={"center"}
        justifyContent={"center"}
        color={"#fbe296"}
        fontSize={`clamp(24px, 8vw, 64px)`}
        lineHeight={"100px"}
        padding={"0 40px"}
        width={"100%"}
        marginBottom={15}
      >
        СУПЕР ИГРА!
      </ShiftHeading>

      <AnimatedHeading
        textAlign={"center"}
        alignSelf={"center"}
        justifyContent={"center"}
        color={"#fbe296"}
        fontSize={`clamp(24px, 8vw, 64px)`}
        lineHeight={"100px"}
        padding={"0 40px"}
        width={"100%"}
      >
        тема:
      </AnimatedHeading>

      <AnimatedHeading
        textAlign={"center"}
        alignSelf={"center"}
        justifyContent={"center"}
        color={"#fbe296"}
        fontSize={`clamp(24px, 8vw, 64px)`}
        lineHeight={"100px"}
        padding={"0 40px"}
        width={"100%"}
      >
        {superGameData?.theme}
      </AnimatedHeading>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100vw;
  height: 100svh;
  background-color: #343dbe;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const appear = keyframes`
  0% {
    opacity: 0;
    transform: translateY(100px);
  }
  100% {
    opacity: 1;
    transform: translateY(0);
  }
`;

const AnimatedHeading = styled(Heading)`
  opacity: 0;
  transform: translateY(100px);
  text-align: center;
  align-self: center;
  justify-content: center;
  animation: ${appear} 1s ease-in-out forwards;
  animation-delay: 3s;
`;

const shift = keyframes`
  0% {
    transform: translateY(100px);
  }
  100% {
    transform: translateY(0);
  }
`;

const ShiftHeading = styled(Heading)`
  transform: translateY(100px);
  animation: ${shift} 1s ease-in-out forwards;
  animation-delay: 3s;
`;

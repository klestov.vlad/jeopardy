import { GamerLayout } from "../../layouts/gamer-layout";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../shared/constants/routes";
import styled, { keyframes } from "styled-components";
import topWave from "./images/top-wave.svg";
import bottomWave from "./images/bottom-wave.svg";
import logoText from "./images/runachpock-logo-text.svg";
import logoIcon from "./images/runachpock-logo-icon.svg";
import { Text } from "@chakra-ui/react";

export const WelcomeScreen = () => {
  const navigate = useNavigate();

  const onStartClick = () => {
    navigate(ROUTES.INTRODUCE_SCREEN);
  };

  return (
    <GamerLayout>
      <BackgroundWrapper>
        <TopWave src={topWave} />
        <BottomWave src={bottomWave} />
        <LogoContainer>
          <LogoIcon src={logoIcon} />
          <Logo src={logoText} />
        </LogoContainer>
      </BackgroundWrapper>
      <ContentWrapper>
        <Button onClick={onStartClick}>
          <Text color={"white"} fontWeight={"bold"} fontSize={32}>
            Начать игру
          </Text>
        </Button>
      </ContentWrapper>
    </GamerLayout>
  );
};

const BackgroundWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

const ContentWrapper = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;

const TopWave = styled.img`
  top: 0;
  left: 0;
  position: absolute;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  width: 100%;
`;

const BottomWave = styled.img`
  bottom: 0;
  left: 0;
  position: absolute;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  width: 100%;
`;

const LogoContainer = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  top: calc(50% - 30px);
  left: 50%;
  transform: translate(-50%, calc(-50% - 50px));
  width: 100%;
`;

const Logo = styled.img`
  width: 100%;
  max-width: 300px;
  margin-top: 33px;
`;

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }`;

const LogoIcon = styled.img`
  width: 100%;
  max-width: 120px;
  animation: ${rotate} 5s linear infinite;
`;

const Button = styled.button`
  width: 100%;
  padding: 10px 20px;
  background-color: #e45ebf;
  border-radius: 10px;
  margin-bottom: 80px;
`;

import styled from "styled-components";
import { Text, Image, Box } from "@chakra-ui/react";
import { GamerLayout } from "../../layouts/gamer-layout";
import logoIcon from "./images/runachpock-logo-icon.svg";

interface UserSpinToWinScreenProps {
  isWinnerIsMe?: boolean;
  setSpinTry: () => void;
  isDemoSpinAvailable?: boolean;
}

export const UserSpinToWinScreen = ({
  isWinnerIsMe,
  setSpinTry,
  isDemoSpinAvailable,
}: UserSpinToWinScreenProps) => {
  return (
    <GamerLayout>
      <BackgroundWrapper>
        <RotationWrapper>
          <StyledImage src="/images/PinClipart.png" alt="background" />
        </RotationWrapper>
      </BackgroundWrapper>

      <ContentWrapper>
        <Box>
          <Text
            fontSize={36}
            color={"white"}
            textAlign={"center"}
            fontWeight={"black"}
            userSelect={"none"}
          >
            Нажми, чтобы
          </Text>
        </Box>
        <Box>
          <Text
            fontSize={36}
            color={"white"}
            textAlign={"center"}
            fontWeight={"black"}
            userSelect={"none"}
          >
            крутить колесо ФОРТУНЫ!
          </Text>
        </Box>
      </ContentWrapper>

      <CentralCircle onClick={setSpinTry}>
        <TextWrapper>
          <Box width={"100%"} height={"100%"} />
          <Box
            position={"absolute"}
            top={"50%"}
            left={"50%"}
            transform={"translate(-50%, -50%)"}
            textAlign={"center"}
            width={"100%"}
            height={"100%"}
            padding={"20px"}
            display={"flex"}
            alignItems={"center"}
            justifyContent={"center"}
            userSelect={"none"}
          >
            {/* <LogoIcon src={logoIcon} /> */}
            <Text
              position={"absolute"}
              fontSize={46}
              textAlign={"center"}
              fontWeight={"black"}
              color={"#E45EBF"}
              userSelect={"none"}
            >
              CHPOCK!
            </Text>
          </Box>
        </TextWrapper>
      </CentralCircle>
    </GamerLayout>
  );
};

const BackgroundWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-color: white;
  overflow: hidden;
`;

const ContentWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding: 70px 0;
  user-select: none;
`;

const StyledImage = styled(Image)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  min-width: 100%;
  min-height: 100%;
  object-fit: contain;
  user-select: none;
`;

const RotationWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const CentralCircle = styled.button`
  width: 253px;
  height: 253px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: translate(-50%, -50%);
  border-radius: 50%;
  background: white;
  border: 7px solid #e45ebf;
  position: absolute;
  top: 50%;
  left: 50%;
  /* filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25)); */
  user-select: none;
  transition: transform 0.2s ease-in-out;
  &:active {
    transform: translate(-50%, -50%) scale(0.75);
    filter: none;
  }
`;

const Logo = styled.img`
  width: 100%;
  max-width: 300px;
  margin-top: 33px;
`;

const TextWrapper = styled.div`
  user-select: none;
  width: 100%;
  height: 100%;
`;

const LogoIcon = styled.img`
  width: 100%;
  height: 100%;
  opacity: 0.13;
`;

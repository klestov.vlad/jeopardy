import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useSharedGameScreen } from "../../shared/hooks/useSharedGameScreen";
import { useNavigate } from "react-router-dom";
import { ActiveGameScreens } from "../../shared/constants/routes";

const SHOW_DELAY = 900;
const DELAY_TO_CATEGORIES = 3000;

export const GamePreviewScreen = () => {
  const { currentScreen, categories } = useSharedGameScreen();
  const navigate = useNavigate();
  const [isVisible, setIsVisible] = useState<{ [index: string]: boolean }>({});
  const [isTitleVisible, setIsTitleVisible] = useState(true);

  useEffect(() => {
    if (!currentScreen) {
      return;
    }

    if (currentScreen !== ActiveGameScreens.PREVIEW_SCREEN) {
      navigate(currentScreen);
    }
  }, [currentScreen, navigate]);

  useEffect(() => {
    if (categories && categories.length > 0) {
      setTimeout(() => {
        setIsTitleVisible(false);
      }, DELAY_TO_CATEGORIES);

      categories.forEach((_, index) => {
        setTimeout(() => {
          setIsVisible((prev) => ({ ...prev, [index]: true }));
        }, index * SHOW_DELAY + 3000);
      });
    }
  }, [categories]);

  return (
    <Wrapper>
      <MainTitle $isTitleVisible={isTitleVisible}>Вопросы Раунда</MainTitle>
      {categories?.map((category, index) => (
        <TitleRound key={index} $isVisible={isVisible[index]}>
          {category}
        </TitleRound>
      ))}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100vw;
  height: 100svh;
  background-color: #343dbe;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

const MainTitle = styled.div<{ $isTitleVisible: Boolean }>`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  opacity: ${({ $isTitleVisible }) => ($isTitleVisible ? 1 : 0)};
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  font-size: 96px;
  color: #fbe296;
  transition: opacity 0.5s ease, transform 0.5s ease;
  text-transform: uppercase;
`;

const TitleRound = styled.div<{ $isVisible: boolean }>`
  opacity: ${({ $isVisible }) => ($isVisible ? 1 : 0)};
  transform: translateY(${({ $isVisible }) => ($isVisible ? "0" : "20px")});
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #343dbe;
  font-size: 48px;
  color: #fbe296;
  transition: opacity 0.5s ease, transform 0.5s ease;
`;

export default GamePreviewScreen;

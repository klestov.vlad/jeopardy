import styled, { css, keyframes } from "styled-components";
import { useSharedGame } from "../../shared/hooks/useSharedGame";
import { Text } from "@chakra-ui/react";
import { QuestionModal } from "../../components/question-screen";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useSharedGameScreen } from "../../shared/hooks/useSharedGameScreen";
import { ActiveGameScreens } from "../../shared/constants/routes";

export const SharedGameScreen = () => {
  const { currentQuestion, questions, gameData } = useSharedGame();
  const { currentScreen } = useSharedGameScreen();
  const navigate = useNavigate();

  const columns = (questions?.prices?.length || 3) + 1;
  const rows = Object.keys(questions?.categories || {})?.length || 3;
  const isQuestionShow = Boolean(gameData?.isQuestionShow);

  const checkIfCellActive = (category: string, price: string) => {
    return (
      gameData?.currentCategory === category &&
      gameData?.currentPrice === price.toString()
    );
  };

  useEffect(() => {
    if (!currentScreen) {
      return;
    }

    if (currentScreen !== ActiveGameScreens.SHARED_GAME_SCREEN) {
      navigate(currentScreen);
    }
  }, [currentScreen, navigate]);

  return (
    <Wrapper $rows={rows}>
      {Object.keys(questions?.categories || {}).map((category) => {
        const questionData = questions?.categories?.[category];
        return (
          <RowsWrapper $columns={columns} key={category}>
            <TitleCell key={category}>
              <Text textAlign={"center"}>{questionData?.name}</Text>
            </TitleCell>
            {questions?.prices.map((price) => {
              return (
                <Cell
                  key={price}
                  $isActive={checkIfCellActive(category, price)}
                >
                  {questionData?.questions?.[price]?.isAnswered ? "" : price}
                </Cell>
              );
            })}
          </RowsWrapper>
        );
      })}
      <QuestionModal
        currentQuestion={currentQuestion}
        isQuestionShow={isQuestionShow}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div<{ $rows: number }>`
  width: 100vw;
  height: 100svh;
  background-color: #fbe296;
  display: grid;
  grid-template-rows: repeat(${({ $rows }) => $rows}, 1fr);
  grid-gap: 1px;
`;

const RowsWrapper = styled.div<{ $columns: number }>`
  display: grid;
  grid-template-columns: ${({ $columns }) =>
    `3fr repeat(${$columns - 1}, 1fr)`};
  grid-gap: 1px;
`;

const blinkAnimation = keyframes`
  0%, 50%, 100% {
    opacity: 1;
  }
  25%, 75% {
    opacity: 0;
  }
`;

const blinkAnimationStyles = css`
  animation: ${blinkAnimation} 1s 2;
`;

const Cell = styled.div<{ $isActive?: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 20px;
  background-color: #343dbe;
  font-size: 48px;
  color: ${({ $isActive }) => ($isActive ? "black" : "#fbe296")};
  color: #fbe296;
  ${({ $isActive }) => ($isActive ? blinkAnimationStyles : "")};
`;

const TitleCell = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #343dbe;
  font-size: 48px;
  color: #fbe296;
`;

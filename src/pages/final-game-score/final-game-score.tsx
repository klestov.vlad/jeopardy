import { useNavigate } from "react-router-dom";
import { useSharedGameScreen } from "../../shared/hooks/useSharedGameScreen";
import { useEffect } from "react";
import { ActiveGameScreens } from "../../shared/constants/routes";
import styled, { keyframes } from "styled-components";
import { useUserGameScore } from "../../shared/hooks/useUserGameScore";
import { Box, Heading, SimpleGrid, Text } from "@chakra-ui/react";
import animationData from "./lottie-animations/animation_lm1pbhrl.json";
import Lottie from "react-lottie";

import cup from "./images/cup.svg";
import podium from "./images/podium.svg";

export const FinalGameScore = () => {
  const { users } = useUserGameScore();
  const { currentScreen } = useSharedGameScreen();
  const navigate = useNavigate();

  useEffect(() => {
    if (!currentScreen) {
      return;
    }

    if (currentScreen !== ActiveGameScreens.FINAL_GAME_SCORE) {
      navigate(currentScreen);
    }
  }, [currentScreen, navigate]);

  const usersByScore = Object.keys(users).sort((a, b) => {
    return Number(users[b]?.count || "0") - Number(users[a]?.count || "0");
  });

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <Wrapper>
      <CupWrapper>
        <Cup src={cup} />
        <Box position={"absolute"} bottom={"-10px"}>
          <AnimatedHeading
            size="3xl"
            textAlign={"center"}
            color={"white"}
            marginTop={2}
            $delay={5}
          >
            Победитель
          </AnimatedHeading>
        </Box>
      </CupWrapper>
      <AnimationWrapper>
        <Lottie options={defaultOptions} />
      </AnimationWrapper>
      <PodiumWrapper>
        <Podium src={podium} />
        <WinnersWrapper>
          <AnimatedHeading
            textAlign={"center"}
            color={"white"}
            marginTop={2}
            $delay={4}
          >
            <Box
              background={"#ECE5FF"}
              padding={"10px 40px"}
              borderRadius={30}
              marginBottom={"80px"}
            >
              <Text color={"#672F58"} fontSize={36}>
                {users[usersByScore[1]]?.name}
              </Text>
            </Box>
          </AnimatedHeading>
          <AnimatedHeading
            textAlign={"center"}
            color={"white"}
            marginTop={2}
            $delay={6}
          >
            <Box
              background={"#ECE5FF"}
              padding={"10px 40px"}
              borderRadius={30}
              marginBottom={"250px"}
            >
              <Text color={"#672F58"} fontSize={48}>
                {users[usersByScore[0]]?.name}
              </Text>
            </Box>
          </AnimatedHeading>
          <AnimatedHeading
            textAlign={"center"}
            color={"white"}
            marginTop={2}
            $delay={2}
            marginBottom={"80px"}
          >
            <Box background={"#ECE5FF"} padding={"10px 40px"} borderRadius={30}>
              <Text color={"#672F58"} fontSize={36}>
                {users[usersByScore[2]]?.name}
              </Text>
            </Box>
          </AnimatedHeading>
        </WinnersWrapper>
      </PodiumWrapper>
      {/* <Box>
        <AnimatedHeading
          size="3xl"
          textAlign={"center"}
          color={"#fbe296"}
          marginTop={2}
          $delay={1}
        >
          Победитель
        </AnimatedHeading>
        <AnimatedHeading
          size="4xl"
          textAlign={"center"}
          color={"#fbe296"}
          marginTop={2}
          $delay={2}
        >
          {users[usersByScore[0]]?.name}
        </AnimatedHeading>
        <AnimatedHeading
          size="4xl"
          textAlign={"center"}
          color={"#fbe296"}
          marginTop={2}
          $delay={3}
        >
          {Number(users[usersByScore[0]]?.count || 0)}
        </AnimatedHeading>
      </Box>
      <AnimatedSimpleGrid spacing={20} columns={2} padding={20}>
        {usersByScore?.slice(1)?.map((userKey, index) => (
          <Box display={"flex"} justifyContent={"center"}>
            <Heading size="xl" textAlign={"center"} color={"#fbe296"}>
              {index + 2}) {users[userKey].name}{" "}
              {Number(users[userKey].count || 0)}
            </Heading>
          </Box>
        ))}
      </AnimatedSimpleGrid> */}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100vw;
  height: 100svh;
  background-color: #bfb3df;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: center;
`;

const appear = keyframes`
  0% {
    opacity: 0;
    transform: translateY(100px);
  }
  100% {
    opacity: 1;
    transform: translateY(0);
  }
`;

const AnimatedHeading = styled(Heading)<{ $delay: number }>`
  display: flex;
  opacity: 0;
  transform: translateY(100px);
  text-align: center;
  align-self: center;
  justify-content: center;
  animation: ${appear} 1s ease-in-out forwards;
  animation-delay: ${({ $delay }) => $delay}s;
`;

const AnimatedSimpleGrid = styled(SimpleGrid)`
  opacity: 0;
  transform: translateY(100px);
  text-align: center;
  align-self: center;
  justify-content: center;
  animation: ${appear} 1s ease-in-out forwards;
  animation-delay: 4s;
`;

const AnimationWrapper = styled.div`
  opacity: 0;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-self: center;
  justify-content: center;
  animation: ${appear} 1s ease-in-out forwards;
  animation-delay: 7s;
  z-index: 2;
`;

const CupWrapper = styled.div`
  opacity: 1;
  position: absolute;
  top: 25px;
  left: 0;
  width: 100%;
  height: 50%;
  display: flex;
  align-self: center;
  justify-content: center;
  z-index: 1;
`;

const Cup = styled.img`
  position: absolute;
  width: 300px;
  object-fit: contain;
`;

const PodiumWrapper = styled.div`
  opacity: 1;
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  display: flex;
  align-self: center;
  justify-content: center;
  flex-shrink: 0;
  z-index: 10;
`;

const Podium = styled.img`
  position: absolute;
  bottom: 0;
  height: 286px;
  width: 1185px;
  object-fit: contain;
  flex-shrink: 0;
`;

const WinnersWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  width: 1185px;
  justify-content: space-around;
  position: absolute;
  bottom: 0;
`;

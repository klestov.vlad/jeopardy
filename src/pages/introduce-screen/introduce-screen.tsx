import styled from "styled-components";
import { useUserAuth } from "../../shared/hooks/useUserAuth";
import { Input } from "@chakra-ui/input";
import { Button, Heading, Text } from "@chakra-ui/react";
import { GamerLayout } from "../../layouts/gamer-layout";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../shared/constants/routes";
import { ArrowRightIcon } from "@chakra-ui/icons";
import leavesImage from "./images/leaves.png";

export const IntroduceScreen = () => {
  const { setUserName, user } = useUserAuth();
  const navigate = useNavigate();

  const onStart = () => {
    navigate(ROUTES.READY_TO_PLAY_SCREEN);
  };

  return (
    <GamerLayout>
      <BackgroundWrapper>
        <TopImage src={leavesImage} />
      </BackgroundWrapper>
      <ContentWrapper>
        <Heading textAlign={"center"} color={"white"} size={"4xl"}>
          Как тебя зовут?
        </Heading>
        <Input
          marginTop={10}
          backgroundColor={"white"}
          height={62}
          fontSize={32}
          placeholder="Введи имя"
          value={user?.name}
          onChange={(e) => setUserName(e.target.value)}
        />
        <Button
          position={"absolute"}
          bottom={12}
          width={"100%"}
          colorScheme="pink"
          variant="outline"
          rightIcon={<ArrowRightIcon />}
          size={"lg"}
          backgroundColor={"white"}
          fontSize={24}
          padding={6}
          isDisabled={!user?.name}
          onClick={onStart}
        >
          Далее
        </Button>
      </ContentWrapper>
    </GamerLayout>
  );
};

const BackgroundWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-color: #e45ebf;
  overflow: hidden;
`;

const ContentWrapper = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const TopImage = styled.img`
  position: absolute;
  top: -15%;
  left: 0;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  width: 100%;
  transform: scale(2);
`;

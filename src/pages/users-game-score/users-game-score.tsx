import { useNavigate } from "react-router-dom";
import { useSharedGameScreen } from "../../shared/hooks/useSharedGameScreen";
import { useEffect } from "react";
import { ActiveGameScreens } from "../../shared/constants/routes";
import styled from "styled-components";
import { useUserGameScore } from "../../shared/hooks/useUserGameScore";
import { Box, Card, CardBody, Heading, SimpleGrid } from "@chakra-ui/react";
import logoIcon from "./images/runachpock-logo-icon.svg";

export const UsersGameScore = () => {
  const { users } = useUserGameScore();
  const { currentScreen } = useSharedGameScreen();
  const navigate = useNavigate();

  useEffect(() => {
    if (!currentScreen) {
      return;
    }

    if (currentScreen !== ActiveGameScreens.USERS_GAME_SCORE) {
      navigate(currentScreen);
    }
  }, [currentScreen, navigate]);

  return (
    <Wrapper>
      <SimpleGrid
        spacing={20}
        templateColumns="repeat(auto-fill, minmax(350px, 1fr))"
        padding={20}
      >
        {Object.keys(users).map((userKey) => (
          <Card
            maxW="sm"
            variant={"elevated"}
            backgroundColor={"#ECE5FF"}
            borderRadius={30}
          >
            <CardBody>
              <Heading size="2xl" textAlign={"center"} color={"#672F58"}>
                {users[userKey].name}
              </Heading>

              <Box padding={4} position={"relative"}>
                <LogoIcon src={logoIcon} />
                <Box
                  border={"3px solid #672F58"}
                  borderRadius={30}
                  backgroundColor={"white"}
                  padding={2}
                >
                  <Heading size="xl" textAlign={"center"} color={"#672F58"}>
                    {Number(users[userKey].count || 0)}
                  </Heading>
                </Box>
              </Box>
            </CardBody>
          </Card>
        ))}
      </SimpleGrid>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100vw;
  height: 100svh;
  background-color: #bfb3df;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: center;
`;

const LogoIcon = styled.img`
  position: absolute;
  height: calc(100% - 32px);
  top: 16px;
  left: 16px;
`;

import styled, { keyframes } from "styled-components";
import { CircularProgress, Image } from "@chakra-ui/react";
import QRCode from "react-qr-code";
import { useWelcomeScreenInfo } from "../../shared/hooks/useWelcomeScreenInfo";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../shared/constants/routes";

export const QrCodeScreen = () => {
  const { sharedUrl, isGameStarted } = useWelcomeScreenInfo();
  const navigation = useNavigate();

  useEffect(() => {
    if (isGameStarted) {
      navigation(ROUTES.PREVIEW_SCREEN);
    }
  }, [isGameStarted, navigation]);

  return (
    <Wrapper>
      <RotationWrapper>
        <StyledImage src="/images/hypno.svg" alt="background" />
      </RotationWrapper>
      <CentralCircle>
        {sharedUrl ? (
          <QRCode
            bgColor={"transparent"}
            fgColor={"white"}
            style={{
              height: "auto",

              width: "60%",
            }}
            value={sharedUrl}
            viewBox={`0 0 256 256`}
          />
        ) : (
          <CircularProgress isIndeterminate color="white" />
        )}
      </CentralCircle>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: relative;
  width: 100vw;
  height: 100svh;
  overflow: hidden;
`;

const rotate = keyframes`
    0% {
        transform: rotate(0deg) scale(1);;
        
    }
    50% {
        transform: rotate(180deg) scale(1.3);
    }
    100% {
        transform: rotate(360deg) scale(1);;
    }
`;

const StyledImage = styled(Image)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) scale(1.5);
  min-width: 100%;
  min-height: 100%;
  object-fit: contain;
`;

const RotationWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  animation: ${rotate} 10s linear infinite;
`;

const CentralCircle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  transform: translate(-50%, -50%);
  width: 80vh;
  height: 80vh;
  border-radius: 50%;
  background-color: #e45ebf;
  position: absolute;
  top: 50%;
  left: 50%;
`;

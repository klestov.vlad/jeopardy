import styled, { keyframes } from "styled-components";
import { useUserAuth } from "../../shared/hooks/useUserAuth";
import { Heading, Text, Image, Box } from "@chakra-ui/react";
import { GamerLayout } from "../../layouts/gamer-layout";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../shared/constants/routes";
import logoText from "./images/runachpock-logo-text.svg";
import { useState } from "react";

export const ReadyToPlayScreen = () => {
  const [isPressed, setIsPressed] = useState(false);
  const [timeoutId, setTimeoutId] = useState<number | null>(null);
  const { user, setUserIsReady } = useUserAuth();
  const navigate = useNavigate();

  const onStart = () => {
    setIsPressed(true);
    const timerId = window.setTimeout(() => {
      setIsPressed(false);
      setUserIsReady(true);
      navigate(ROUTES.GAME_SCREEN);
    }, 2500);
    setTimeoutId(timerId);
  };

  const onStop = (timeoutId: number | null) => {
    setIsPressed(false);
    if (timeoutId) {
      window.clearTimeout(timeoutId);
    }
  };

  return (
    <GamerLayout>
      <BackgroundWrapper $isPressed={isPressed}>
        <RotationWrapper $isPressed={isPressed}>
          <StyledImage src="/images/hypno.svg" alt="background" />
        </RotationWrapper>
      </BackgroundWrapper>

      <ContentWrapper>
        <Box>
          <Text
            fontSize={36}
            color={"#6F3F61"}
            textAlign={"center"}
            fontWeight={"black"}
          >
            Игрок
          </Text>
          <Text
            marginTop={-6}
            fontSize={48}
            color={"#E45EBF"}
            textAlign={"center"}
            fontWeight={"black"}
          >
            {user?.name}
          </Text>
        </Box>
        <Logo src={logoText} />
      </ContentWrapper>

      <CentralCircle
        onMouseDown={onStart}
        onMouseUp={() => onStop(timeoutId)}
        onMouseLeave={() => onStop(timeoutId)}
        onTouchStart={onStart}
        onTouchEnd={() => onStop(timeoutId)}
        $isPressed={isPressed}
      >
        <TextWrapper $isPressed={isPressed}>
          <Box
            position={"absolute"}
            top={"50%"}
            left={"50%"}
            transform={"translate(-50%, -50%)"}
            textAlign={"center"}
          >
            <Heading fontSize={64} color={"white"} userSelect={"none"}>
              Зажми
            </Heading>
            <Text
              fontSize={36}
              fontWeight="bold"
              color={"white"}
              userSelect={"none"}
            >
              если готов
            </Text>
          </Box>
        </TextWrapper>
      </CentralCircle>
    </GamerLayout>
  );
};

const changeColor = keyframes`
    0% {
      background-color: white;
    }
    50% {
      background-color: #e45ebf;
    }
    100% {
      background-color: #e45ebf;
    }
`;

const BackgroundWrapper = styled.div<{
  $isPressed: boolean;
}>`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-color: white;
  overflow: hidden;

  animation: ${({ $isPressed }) => ($isPressed ? changeColor : "none")} 2.5s
    linear infinite;
`;

const ContentWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding: 70px 0;
  user-select: none;
`;

const StyledImage = styled(Image)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) scale(1.5);
  min-width: 100%;
  min-height: 100%;
  object-fit: contain;
`;

const rotate = keyframes`
    0% {
        transform: rotate(0deg) scale(1.6);
        
    }
    50% {
        transform: rotate(180deg) scale(1.6);
    }
    100% {
        transform: rotate(360deg) scale(1.6);
    }
`;

const scaleCircle = keyframes`
    0% {
        width: 300px;
        height: 300px;
        
    }
    100% {
        width: 1000px;
        height: 1000px;
    }
`;

const RotationWrapper = styled.div<{ $isPressed: boolean }>`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: scale(1.6);

  animation: ${({ $isPressed }) => ($isPressed ? rotate : "none")} 2.5s linear
    infinite;

  opacity: 0.5;
`;

const CentralCircle = styled.div<{ $isPressed: boolean }>`
  width: 300px;
  height: 300px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: translate(-50%, -50%);
  border-radius: 50%;
  background-color: #e45ebf;
  position: absolute;
  top: 50%;
  left: 50%;

  /* animation: ${scaleCircle} 10s linear infinite;
  animation-play-state: ${({ $isPressed }) =>
    $isPressed ? "running" : "paused"}; */

  animation: ${({ $isPressed }) => ($isPressed ? scaleCircle : "none")} 2.5s
    linear infinite;
`;

const Logo = styled.img`
  width: 100%;
  max-width: 300px;
  margin-top: 33px;
`;

const opacity = keyframes`
    0% {
      opacity: 1;
    }
    75% {
      opacity: 0;
    }
    100% {
      opacity: 0;
    }
`;

const TextWrapper = styled.div<{ $isPressed: boolean }>`
  animation: ${({ $isPressed }) => ($isPressed ? opacity : "none")} 2.5s linear;
  user-select: none;
`;

import {
  QuestionsControl,
  RoundControl,
  SharedUrlControl,
  UserControl,
} from "../../components/game-host-screen";
import { CurrentQuestionControl } from "../../components/game-host-screen/current-question-control/current-question-control";
import { SuperGameControl } from "../../components/game-host-screen/super-game-control/super-game-control";
import { MainLayout } from "../../layouts";
import { GameControlProvider } from "../../providers/game-control-provider";

export const GameHostScreen = () => {
  return (
    <GameControlProvider>
      <MainLayout title="Game host control">
        <UserControl />
        <RoundControl />
        <CurrentQuestionControl />
        <QuestionsControl />
        <SuperGameControl />
        <SharedUrlControl />
      </MainLayout>
    </GameControlProvider>
  );
};

import React, { useEffect } from "react";
import styled from "styled-components";
import { Image } from "@chakra-ui/react";
import spinBackground from "./images/spin-background.svg";
import spinner from "./images/spinner.svg";
import pointer from "./images/pointer.svg";
import { ActiveGameScreens } from "../../shared/constants/routes";
import { useNavigate } from "react-router-dom";
import { useSharedGameScreen } from "../../shared/hooks/useSharedGameScreen";
import { useAnimationFrame } from "./hooks/useAnimationFrame";
import { useUserSpinToWin } from "../../shared/hooks/useUserSpinToWin";

const priceList = [
  "10 000 бат",
  "футблоку Runachpok",
  "Чупа-чупс",
  "Пиво Chang",
  "Билет на пинг-понг шоу",
  "Книгу 'Дон Кихот'",
  "Крабик для волос (с ароматом)",
  "Keпку Runachpok",
];

export const SpinToWin = () => {
  const [offset, setOffset] = React.useState(0);
  const [count, setCount] = React.useState(200);

  const {
    users,
    isFirstTimeSpinAvailable,
    isDemoSpinAvailable,
    isSpinStarted,
    setSpinTry,
  } = useUserSpinToWin();
  const { currentScreen } = useSharedGameScreen();
  const navigate = useNavigate();

  const usersByScore = Object.keys(users).sort((a, b) => {
    return Number(users[b]?.count || "0") - Number(users[a]?.count || "0");
  });

  useEffect(() => {
    if (!currentScreen) {
      return;
    }

    if (currentScreen !== ActiveGameScreens.SPIN_TO_WIN_SCREEN) {
      navigate(currentScreen);
    }
  }, [currentScreen, navigate]);

  useAnimationFrame((deltaTime) => {
    setCount((prevCount) =>
      prevCount < 150
        ? prevCount > 99.5
          ? 100
          : (prevCount + deltaTime * 0.005) % 100
        : prevCount
    );
  });

  const startSpin = () => {
    setCount(0);
    isDemoSpinAvailable ? setOffset(Math.random() * 360) : setOffset(0);
    setSpinTry(false);
  };

  useEffect(() => {
    if (!isFirstTimeSpinAvailable && !isDemoSpinAvailable) return;
    if (!isSpinStarted) return;
    startSpin();
  }, [isSpinStarted]);

  function calculateRotation(input: number): number {
    const ease = (t: number) => Math.pow(-2 * t + 2, 4) / 15;

    const easedInput = ease(input / 100);

    const rotation = easedInput * 2500;
    return rotation;
  }

  const currentAngle =
    count > 100 ? count : calculateRotation(count) - 3 + offset;

  const getCurrentPrice = () => {
    const index =
      currentAngle > 0
        ? Math.floor((currentAngle % 360) / 45)
        : Math.floor((360 + (currentAngle % 360)) / 45);
    return priceList[index];
  };

  return (
    <Wrapper>
      <RotationWrapper>
        <StyledImage src="/images/hypno.svg" alt="background" />
        <SpinnerWrapper>
          <SpinBackground src={spinBackground} />
          <Spinner src={spinner} $currentAngle={currentAngle} />
          <Pointer src={pointer} />
        </SpinnerWrapper>
      </RotationWrapper>
      <InfoWrapper>
        <WinnerRow>
          <Name>{users[usersByScore[0]]?.name} </Name>выигрывает
        </WinnerRow>
        <Price>{count < 150 ? getCurrentPrice() : `???`}</Price>
      </InfoWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  width: 100vw;
  height: 100svh;
  overflow: hidden;
  background-color: #eee9fb;
`;

const StyledImage = styled(Image)`
  position: absolute;
  top: 50%;
  left: 40%;
  transform: translate(-50%, -50%) scale(1.5);
  min-width: 100%;
  min-height: 100%;
  object-fit: contain;
  opacity: 0.1;
`;

const RotationWrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const SpinnerWrapper = styled.div`
  position: relative;
  left: 10%;
  height: 100%;
  width: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const SpinBackground = styled.img`
  position: absolute;
  height: 100%;
`;

const Spinner = styled.img<{ $currentAngle: number }>`
  position: absolute;
  max-width: 90%;
  max-height: 90%;
  transform: rotate(${({ $currentAngle }) => $currentAngle}deg);
`;

const Pointer = styled.img`
  position: absolute;
  max-width: 12%;
  max-height: 12%;
  right: 0;
`;

const InfoWrapper = styled.div`
  position: absolute;
  right: 5%;
  background-color: white;
  padding: 20px;
  filter: drop-shadow(10px 10px 40px rgba(0, 0, 0, 0.25));
  border-radius: 30px;
  width: 30%;
`;

const WinnerRow = styled.p`
  font-size: 24px;
  font-weight: 500;
  color: #672f58;
  text-align: center;
`;

const Name = styled.span`
  font-size: 36px;
  color: #e45ebf;
  font-weight: 700;
`;

const Price = styled.p`
  font-size: 48px;
  font-weight: 500;
  color: #672f58;
  text-align: center;
`;

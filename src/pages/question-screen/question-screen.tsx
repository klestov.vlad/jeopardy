import { styled } from "styled-components";
import { useEditingQuestions } from "../../shared/hooks/useEditingQuestions";
import { Accordion, Button } from "@chakra-ui/react";
import { MainLayout } from "../../layouts";
import { Round } from "../../components/question-screen";
import { SuperGameQuestion } from "../../components/question-screen/super-game-question/super-game-question";

export const QuestionScreen = () => {
  const {
    rounds,
    superGame,
    addNewRound,
    deleteRound,
    setPrices,
    addCategory,
    deleteCategory,
    setQuestion,
    editCategoryName,
    deleteQuestion,
    setCatInABagStatus,
    setAuctionQuestionStatus,
    setSuperGameQuestion,
    setSuperGameAnswer,
    setSuperGameTheme,
  } = useEditingQuestions();
  return (
    <MainLayout title="Question Screen">
      <Accordion allowToggle defaultIndex={[0]}>
        {Object.keys(rounds || {}).map((roundId, index) => {
          return (
            <Round
              key={roundId}
              roundId={roundId}
              index={index}
              round={rounds[roundId]}
              deleteRound={deleteRound}
              setPrices={setPrices}
              addCategory={addCategory}
              deleteCategory={deleteCategory}
              setQuestion={setQuestion}
              editCategoryName={editCategoryName}
              deleteQuestion={deleteQuestion}
              setCatInABagStatus={setCatInABagStatus}
              setAuctionQuestionStatus={setAuctionQuestionStatus}
            />
          );
        })}
      </Accordion>
      <SuperGameQuestion
        setSuperGameQuestion={setSuperGameQuestion}
        setSuperGameAnswer={setSuperGameAnswer}
        setSuperGameTheme={setSuperGameTheme}
        superGame={superGame}
      />
      <NewRoundButtonWrapper>
        <Button colorScheme="teal" onClick={addNewRound}>
          + Add round
        </Button>
      </NewRoundButtonWrapper>
    </MainLayout>
  );
};

const NewRoundButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 20px;
`;

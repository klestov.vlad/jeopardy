import { useNavigate } from "react-router-dom";
import { useSharedGameScreen } from "../../shared/hooks/useSharedGameScreen";
import { useEffect } from "react";
import { ActiveGameScreens } from "../../shared/constants/routes";
import styled, { keyframes } from "styled-components";
import { useUserGameScore } from "../../shared/hooks/useUserGameScore";
import { Card, CardBody, Heading, SimpleGrid } from "@chakra-ui/react";

export const SuperGameAnswers = () => {
  const { users } = useUserGameScore();
  const { currentScreen } = useSharedGameScreen();
  const navigate = useNavigate();

  useEffect(() => {
    if (!currentScreen) {
      return;
    }

    if (currentScreen !== ActiveGameScreens.USERS_GAME_SCORE) {
      navigate(currentScreen);
    }
  }, [currentScreen, navigate]);

  return (
    <Wrapper>
      <SimpleGrid spacing={2} padding={2} columns={3}>
        {Object.keys(users).map((userKey, index) => (
          <AnimatedCard
            $delay={index * 6}
            maxW="sm"
            variant={"elevated"}
            backgroundColor={"#343dbe"}
          >
            <CardBody>
              <Heading size="xl" textAlign={"center"} color={"#fbe296"}>
                {users[userKey].name}
              </Heading>
              <AnimatedHeading
                $delay={index * 6 + 2}
                size="xl"
                textAlign={"center"}
                color={"#fbe296"}
                marginTop={2}
              >
                ответ: {users[userKey].auctionAnswer}
              </AnimatedHeading>
              <AnimatedHeading
                $delay={index * 6 + 4}
                size="xl"
                textAlign={"center"}
                color={"#fbe296"}
                marginTop={2}
              >
                ставка: {Number(users[userKey].auctionBid || 0)}
              </AnimatedHeading>
            </CardBody>
          </AnimatedCard>
        ))}
      </SimpleGrid>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100vw;
  height: 100svh;
  background-color: #343dbe;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: center;
`;

const appear = keyframes`
  from {
    opacity: 0;
    transform: translateY(100px);
  }
  to {
    opacity: 1;
    transform: translateY(0);
  }
`;

const AnimatedCard = styled(Card)<{ $delay: number }>`
  opacity: 0;
  animation: ${appear} 0.5s ease-in-out forwards;
  animation-delay: ${({ $delay }) => $delay}s;
`;

const AnimatedHeading = styled(Heading)<{ $delay: number }>`
  opacity: 0;
  animation: ${appear} 0.5s ease-in-out forwards;
  animation-delay: ${({ $delay }) => $delay}s;
`;

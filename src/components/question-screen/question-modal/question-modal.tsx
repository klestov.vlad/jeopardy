import {
  Heading,
  Image,
  Modal,
  ModalBody,
  ModalContent,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import { useEffect, useRef } from "react";
import { QuestionItemType } from "../../../shared/types";
import styled, { keyframes } from "styled-components";

let timer: NodeJS.Timeout;

interface QuestionModalProps {
  currentQuestion: QuestionItemType | undefined;
  isQuestionShow: boolean;
}

export const QuestionModal = ({
  currentQuestion,
  isQuestionShow,
}: QuestionModalProps) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { text, url } = currentQuestion?.question || {};

  const isCatInABag = currentQuestion?.isCatInABag;
  const isAuctionQuestion = currentQuestion?.isAuctionQuestion;

  const catSound = new Audio("/sounds/si_cat.mp3");
  const auctionSound = new Audio("/sounds/si_auction.mp3");

  const isSoundPlayed = useRef(false);

  useEffect(() => {
    if (currentQuestion) {
      timer = setTimeout(() => {
        onOpen();
        isCatInABag && catSound.play();
        isAuctionQuestion && auctionSound.play();
      }, 1500);
    } else {
      onClose();
      clearTimeout(timer);
    }

    return () => {
      clearTimeout(timer);
      isSoundPlayed.current = false;
    };
  }, [text]);

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      closeOnOverlayClick={false}
      size={"full"}
      isCentered
    >
      <ModalOverlay />
      <ModalContent display={"flex"} backgroundColor={"#343DBE"}>
        <ModalBody display="flex" flexDir={"column"} justifyContent={"center"}>
          {isCatInABag && !isQuestionShow ? (
            <AnimatedHeading
              textAlign={"center"}
              alignSelf={"center"}
              justifyContent={"center"}
              color={"#fbe296"}
              fontSize={200}
              padding={"0 40px"}
            >
              КОТ В <br /> МЕШКЕ
            </AnimatedHeading>
          ) : isAuctionQuestion && !isQuestionShow ? (
            <AnimatedHeading
              textAlign={"center"}
              alignSelf={"center"}
              justifyContent={"center"}
              color={"#fbe296"}
              fontSize={200}
              padding={"0 40px"}
            >
              ВОПРОС <br /> АУКЦИОН
            </AnimatedHeading>
          ) : (
            <>
              {url && (
                <Image
                  src={url}
                  height="100px"
                  flexGrow={1}
                  objectFit={"contain"}
                  margin={"auto"}
                />
              )}
              <Heading
                textAlign={"center"}
                alignSelf={"center"}
                justifyContent={"center"}
                color={"#fbe296"}
                fontSize={`clamp(24px, 8vw, 64px)`}
                lineHeight={"100px"}
                padding={"0 40px"}
              >
                {text}
              </Heading>
            </>
          )}
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

const colorChange = keyframes`
  0% {
    -webkit-transform: perspective(800px) rotateY(0);
            transform: perspective(800px) rotateY(0);
  }
  100% {
    -webkit-transform: perspective(800px) rotateY(360deg);
            transform: perspective(800px) rotateY(360deg);
  }
`;

const AnimatedHeading = styled(Heading)`
  text-align: center;
  align-self: center;
  justify-content: center;
  font-size: 200px;
  padding: 0 40px;
  animation: ${colorChange} 3s 2;
`;

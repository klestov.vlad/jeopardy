import {
  Box,
  ButtonGroup,
  CardBody,
  CardFooter,
  Divider,
  Highlight,
  Stack,
  Text,
  Image,
  Card,
  Tag,
  Checkbox,
} from "@chakra-ui/react";
import { QuestionItemType } from "../../../shared/types";
import { AlertButton } from "../../../ui";
import { EditQuestion } from "../edit-question/edit-question";

interface QuestionCardProps {
  question: QuestionItemType;
  price: string;
  deleteQuestion: () => void;
  setQuestion: (question: QuestionItemType) => void;
  setCatInABagStatus: (status: boolean) => void;
  setAuctionQuestionStatus: (status: boolean) => void;
}

export const QuestionCard = ({
  question,
  price,
  deleteQuestion,
  setQuestion,
  setCatInABagStatus,
  setAuctionQuestionStatus,
}: QuestionCardProps) => {
  return (
    <Card maxW="md" minW="md">
      <CardBody>
        <Box marginBottom={4}>
          <Tag size="lg" variant="solid" colorScheme="blue">
            {price}
          </Tag>
        </Box>
        {question.question.url && (
          <Image
            src={question.question.url}
            alt="Green double couch with wooden legs"
            borderRadius="lg"
          />
        )}
        <Stack mt="6" spacing="3">
          <Text>
            <Highlight
              query="Q:"
              styles={{ px: "2", py: "1", rounded: "full", bg: "blue.100" }}
            >
              {`${"Q: "}` + question.question.text}
            </Highlight>
          </Text>
          {question.answer.url && (
            <Image
              src={question.answer.url}
              alt="Green double couch with wooden legs"
              borderRadius="lg"
            />
          )}
          <Text>
            <Highlight
              query="A:"
              styles={{ px: "2", py: "1", rounded: "full", bg: "green.100" }}
            >
              {`${"A: "}` + question.answer.text}
            </Highlight>
          </Text>
          <Box display={"flex"} marginTop={4} gap={2}>
            <Checkbox
              disabled={question.isAuctionQuestion}
              size="lg"
              colorScheme="orange"
              isChecked={question.isCatInABag}
              onChange={(e) => setCatInABagStatus(e.target.checked)}
            />
            <Text>Question is cat in a bag</Text>
          </Box>
          <Box display={"flex"} marginTop={4} gap={2}>
            <Checkbox
              disabled={question.isCatInABag}
              size="lg"
              colorScheme="orange"
              isChecked={question.isAuctionQuestion}
              onChange={(e) => setAuctionQuestionStatus(e.target.checked)}
            />
            <Text>Question is auction question</Text>
          </Box>
        </Stack>
      </CardBody>
      <Divider />
      <CardFooter width="100%" justifyContent="flex-end" display="flex">
        <ButtonGroup justifyContent="flex-end" display="flex">
          <EditQuestion
            initialQuestionData={question}
            price={price}
            onAddQuestion={(question: QuestionItemType) =>
              setQuestion(question)
            }
          />

          <AlertButton
            title="Delete question"
            buttonName="Delete"
            description="Are you sure you want to delete this question?"
            onConfirm={deleteQuestion}
            cancelButtonText="Cancel"
            confirmButtonText="Delete"
            variant="ghost"
          />
        </ButtonGroup>
      </CardFooter>
    </Card>
  );
};

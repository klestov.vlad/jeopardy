import { Box, FormLabel, Input, Textarea } from "@chakra-ui/react";
import { DrawerModal } from "../../../ui";
import { QuestionItemType } from "../../../shared/types";
import { useRef, useState } from "react";

interface QuestionDrawelProps {
  isOpen: boolean;
  price: string;
  questionData: QuestionItemType;

  onClose: () => void;
  onSave: () => void;
  setQuestionData: React.Dispatch<React.SetStateAction<QuestionItemType>>;
}

export const QuestionDrawer = ({
  isOpen,
  price,
  questionData,

  onClose,
  onSave,
  setQuestionData,
}: QuestionDrawelProps) => {
  const firstField = useRef<HTMLTextAreaElement>(null);

  const isDisabled =
    questionData.question.text === "" || questionData.answer.text === "";

  return (
    <DrawerModal
      isOpen={isOpen}
      onClose={onClose}
      title={`Add question for ${price} points`}
      applyButtonText="Save"
      onApply={onSave}
      isApplyDisabled={isDisabled}
    >
      <Box>
        <FormLabel htmlFor="question">Question</FormLabel>
        <Textarea
          value={questionData.question.text}
          onChange={(e) =>
            setQuestionData({
              ...questionData,
              question: {
                ...questionData.question,
                text: e.target.value,
              },
            })
          }
          ref={firstField}
          id="Question"
          placeholder="Please enter question"
          resize="vertical"
        />
      </Box>

      <Box>
        <FormLabel htmlFor="question url">
          Question image url (optional)
        </FormLabel>
        <Input
          value={questionData.question.url}
          onChange={(e) =>
            setQuestionData({
              ...questionData,
              question: {
                ...questionData.question,
                url: e.target.value,
              },
            })
          }
          id="QuestionUrl"
          placeholder="Please enter url of question image"
          resize="vertical"
        />
      </Box>

      <Box>
        <FormLabel htmlFor="answer">Answer</FormLabel>
        <Textarea
          value={questionData.answer.text}
          onChange={(e) =>
            setQuestionData({
              ...questionData,
              answer: {
                ...questionData.answer,
                text: e.target.value,
              },
            })
          }
          id="Answer"
          placeholder="Please enter answer"
          resize="vertical"
        />
      </Box>

      <Box>
        <FormLabel htmlFor="answerUrl">Answer image url (optional)</FormLabel>
        <Input
          value={questionData.answer.url}
          onChange={(e) =>
            setQuestionData({
              ...questionData,
              answer: {
                ...questionData.answer,
                url: e.target.value,
              },
            })
          }
          id="AnswerUrl"
          placeholder="Please enter url of answer image"
          resize="vertical"
        />
      </Box>
    </DrawerModal>
  );
};

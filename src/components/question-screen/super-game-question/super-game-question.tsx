import { Heading, Input, Text, Textarea } from "@chakra-ui/react";
import { styled } from "styled-components";
import { SuperGameType } from "../../../shared/types";

interface SuperGameQuestionProps {
  setSuperGameQuestion: (value: string) => void;
  setSuperGameAnswer: (value: string) => void;
  setSuperGameTheme: (value: string) => void;
  superGame: SuperGameType;
}

export const SuperGameQuestion = ({
  superGame,
  setSuperGameAnswer,
  setSuperGameQuestion,
  setSuperGameTheme,
}: SuperGameQuestionProps) => {
  return (
    <Wrapper>
      <Heading as="h5" size="md" marginTop={10}>
        Super game question
      </Heading>
      <>
        <Text mb="8px" marginTop={5} fontWeight={"bold"}>
          Theme:
        </Text>
        <Input
          value={superGame.theme}
          onChange={(e) => setSuperGameTheme(e.target.value)}
          placeholder="Super game question theme"
          size="sm"
        />
      </>
      <>
        <Text mb="8px" marginTop={5} fontWeight={"bold"}>
          Question:
        </Text>
        <Textarea
          value={superGame?.question}
          onChange={(e) => setSuperGameQuestion(e.target.value)}
          placeholder="Super game question"
          size="sm"
        />
      </>
      <>
        <Text mb="8px" marginTop={5} fontWeight={"bold"}>
          Answer:
        </Text>
        <Input
          value={superGame.answer}
          onChange={(e) => setSuperGameAnswer(e.target.value)}
          placeholder="Super game answer"
          size="sm"
        />
      </>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  max-width: 500px;
`;

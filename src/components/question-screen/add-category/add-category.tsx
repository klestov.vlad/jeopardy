import {
  Button,
  FormControl,
  FormLabel,
  Input,
  useDisclosure,
} from "@chakra-ui/react";
import { useRef, useState } from "react";
import { ModalWindow } from "../../../ui/modal-window/modal-window";

interface AddCategoryProps {
  addCategory: (categoryName: string) => void;
}

export const AddCategory = ({ addCategory }: AddCategoryProps) => {
  const [categoryName, setCategoryName] = useState("");
  const { isOpen, onOpen, onClose } = useDisclosure();

  const initialRef = useRef(null);

  const onApply = () => {
    addCategory(categoryName);
    setCategoryName("");
    onClose();
  };

  return (
    <>
      <Button onClick={onOpen}>Create category</Button>

      <ModalWindow
        isOpen={isOpen}
        onClose={onClose}
        onApply={onApply}
        title="Create category"
        applyButtonText="Save"
        isApplyDisabled={!categoryName}
      >
        <FormControl>
          <FormLabel>Category name</FormLabel>
          <Input
            ref={initialRef}
            placeholder="Category name"
            value={categoryName}
            onChange={(e) => setCategoryName(e.target.value)}
          />
        </FormControl>
      </ModalWindow>
    </>
  );
};

import styled from "styled-components";
import { QuestionCard } from "../question-card/question-card";
import { Box, Stack } from "@chakra-ui/react";
import { AddQuestion } from "../add-question/add-question";
import { QuestionItemType } from "../../../shared/types";

interface QuestionBlockProps {
  categoriesPrices: string[];
  questions: {
    [price: string]: QuestionItemType;
  };
  setQuestion: (price: string, question: QuestionItemType) => void;
  deleteQuestion: (price: string) => void;
  setCatInABagStatus: (price: string, status: boolean) => void;
  setAuctionQuestionStatus: (price: string, status: boolean) => void;
}

export const QuestionBlock = ({
  categoriesPrices,
  questions,
  setQuestion,
  deleteQuestion,
  setCatInABagStatus,
  setAuctionQuestionStatus,
}: QuestionBlockProps) => {
  return (
    <Box>
      <PricesWrapper>
        {categoriesPrices.map((price, index) => (
          <AddQuestion
            key={price + index}
            price={price}
            onAddQuestion={(question: QuestionItemType) =>
              setQuestion(price, question)
            }
            isQuestionExist={Boolean(questions?.[price])}
          />
        ))}
      </PricesWrapper>
      <Stack
        spacing={4}
        direction="row"
        align="top"
        marginTop={4}
        flexWrap={"wrap"}
      >
        {Object.keys(questions || {}).map((price) => (
          <QuestionCard
            key={price}
            question={questions[price]}
            price={price}
            deleteQuestion={() => deleteQuestion(price)}
            setQuestion={(question: QuestionItemType) =>
              setQuestion(price, question)
            }
            setCatInABagStatus={(status: boolean) =>
              setCatInABagStatus(price, status)
            }
            setAuctionQuestionStatus={(status: boolean) =>
              setAuctionQuestionStatus(price, status)
            }
          />
        ))}
      </Stack>
    </Box>
  );
};

const PricesWrapper = styled.div`
  margin-top: 16px;
  display: flex;
  gap: 16px;
`;

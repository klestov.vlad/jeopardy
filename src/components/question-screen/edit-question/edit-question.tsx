import { AddIcon, CheckCircleIcon } from "@chakra-ui/icons";
import { Button, useDisclosure } from "@chakra-ui/react";
import { QuestionItemType } from "../../../shared/types";
import { useState } from "react";
import { QuestionDrawer } from "../question-drawer/question-drawer";

interface AddQuestionProps {
  onAddQuestion: (question: QuestionItemType) => void;
  initialQuestionData: QuestionItemType;
  price: string;
}

export const EditQuestion = ({
  price,
  initialQuestionData,
  onAddQuestion,
}: AddQuestionProps) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const [questionData, setQuestionData] =
    useState<QuestionItemType>(initialQuestionData);

  const onSave = () => {
    onAddQuestion(questionData);
    onClose();
  };

  return (
    <>
      <Button variant="ghost" colorScheme="blue" onClick={onOpen}>
        Edit
      </Button>

      <QuestionDrawer
        isOpen={isOpen}
        onClose={onClose}
        onSave={onSave}
        price={price}
        questionData={questionData}
        setQuestionData={setQuestionData}
      />
    </>
  );
};

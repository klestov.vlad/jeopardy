import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Heading,
} from "@chakra-ui/react";
import { styled } from "styled-components";
import { QuestionBlock } from "../question-block/question-block";
import { CategoryType, QuestionItemType } from "../../../shared/types";
import { AlertButton } from "../../../ui";
import { SettingsIcon } from "@chakra-ui/icons";
import { OpenCategorySettings } from "../category-settings/category-setting";

interface CategoriesProps {
  categoriesPrices: string[];
  categories?: {
    [categoryKey: string]: CategoryType;
  };
  deleteCategory: (categoryId: string) => void;
  setQuestion: (
    categoryId: string,
    price: string,
    question: QuestionItemType
  ) => void;
  editCategoryName: (categoryId: string, newName: string) => void;
  deleteQuestion: (categoryId: string, price: string) => void;
  setCatInABagStatus: (
    categoryId: string,
    price: string,
    status: boolean
  ) => void;
  setAuctionQuestionStatus: (
    categoryId: string,
    price: string,
    status: boolean
  ) => void;
}

export const Categories = ({
  categoriesPrices,
  categories,
  deleteCategory,
  setQuestion,
  editCategoryName,
  deleteQuestion,
  setCatInABagStatus,
  setAuctionQuestionStatus,
}: CategoriesProps) => {
  return (
    <Box marginTop={4}>
      <Accordion allowToggle defaultIndex={[0]}>
        {categories &&
          Object.keys(categories || {}).map((categoryKey) => {
            return (
              <AccordionItem key={categoryKey}>
                <AccordionButton>
                  <Box as="span" flex="1" textAlign="left">
                    <Heading as="h5" size="sm">
                      {categories[categoryKey]?.name}
                    </Heading>
                  </Box>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel pb={4}>
                  <QuestionBlock
                    categoriesPrices={categoriesPrices}
                    questions={categories[categoryKey].questions}
                    setQuestion={(price: string, question: QuestionItemType) =>
                      setQuestion(categoryKey, price, question)
                    }
                    deleteQuestion={(price: string) =>
                      deleteQuestion(categoryKey, price)
                    }
                    setCatInABagStatus={(price: string, status: boolean) =>
                      setCatInABagStatus(categoryKey, price, status)
                    }
                    setAuctionQuestionStatus={(
                      price: string,
                      status: boolean
                    ) => setAuctionQuestionStatus(categoryKey, price, status)}
                  />
                  <Box marginTop={4} justifyContent="flex-end" display={"flex"}>
                    <OpenCategorySettings
                      editCategoryName={(newName: string) =>
                        editCategoryName(categoryKey, newName)
                      }
                      initialName={categories[categoryKey]?.name}
                    />
                    <AlertButton
                      title="Delete category"
                      buttonName="Delete category"
                      description="Are you sure you want to delete this category?"
                      onConfirm={() => deleteCategory(categoryKey)}
                      cancelButtonText="Cancel"
                      confirmButtonText="Delete"
                    />
                  </Box>
                </AccordionPanel>
              </AccordionItem>
            );
          })}
      </Accordion>
    </Box>
  );
};

import { AddIcon, CheckCircleIcon } from "@chakra-ui/icons";
import { Button, useDisclosure } from "@chakra-ui/react";
import { QuestionItemType } from "../../../shared/types";
import { useState } from "react";
import { QuestionDrawer } from "../question-drawer/question-drawer";

interface AddQuestionProps {
  onAddQuestion: (question: QuestionItemType) => void;
  isQuestionExist?: boolean;
  price: string;
}

export const AddQuestion = ({
  price,
  isQuestionExist,
  onAddQuestion,
}: AddQuestionProps) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const [questionData, setQuestionData] = useState<QuestionItemType>({
    question: {
      text: "",
    },
    answer: {
      text: "",
    },
  });

  const onSave = () => {
    onAddQuestion(questionData);
    onClose();
  };

  return (
    <>
      <Button
        onClick={onOpen}
        leftIcon={isQuestionExist ? <CheckCircleIcon /> : <AddIcon />}
        colorScheme="teal"
        variant="solid"
        isDisabled={isQuestionExist}
      >
        {price}
      </Button>

      <QuestionDrawer
        isOpen={isOpen}
        onClose={onClose}
        onSave={onSave}
        price={price}
        questionData={questionData}
        setQuestionData={setQuestionData}
      />
    </>
  );
};

import React, { useState } from "react";
import {
  ChakraProvider,
  Box,
  Button,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  Stack,
  IconButton,
} from "@chakra-ui/react";
import { AddIcon, DeleteIcon } from "@chakra-ui/icons";

interface ConstEditFormProps {
  onSave: () => void;
  setPrices: React.Dispatch<React.SetStateAction<number[]>>;
  prices: number[];
}

export const ConstEditForm = ({
  prices,
  setPrices,
  onSave,
}: ConstEditFormProps) => {
  const handleNumberChange = (index: number, value: number) => {
    const newNumbers = [...prices];
    newNumbers[index] = value;
    setPrices(newNumbers);
  };

  const handleAddField = () => {
    if (prices?.length < 7) {
      setPrices([...prices, prices[prices?.length - 1] + 100]);
    }
  };

  const handleRemoveField = (index: number) => {
    if (prices?.length > 5) {
      const newNumbers = [...prices];
      newNumbers.splice(index, 1);
      setPrices(newNumbers);
    }
  };

  return (
    <ChakraProvider>
      <Stack spacing={2} marginTop={4}>
        {prices.map((number, index) => (
          <Box key={index} display="flex" alignItems="center">
            <NumberInput
              step={100}
              min={0}
              value={number}
              onChange={(e) =>
                handleNumberChange(index, parseInt(e) ? parseInt(e) : 0)
              }
              placeholder={`Number ${index + 1}`}
              mr={2}
            >
              <NumberInputField />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
            {index >= 5 && (
              <IconButton
                colorScheme="red"
                onClick={() => handleRemoveField(index)}
                aria-label="remove-field"
                icon={<DeleteIcon />}
              />
            )}
          </Box>
        ))}
      </Stack>

      {prices?.length < 7 && (
        <IconButton
          marginTop={2}
          onClick={handleAddField}
          aria-label="add-field"
          icon={<AddIcon />}
        />
      )}

      <Box marginTop={2}>
        <Button colorScheme="teal" onClick={onSave}>
          Save
        </Button>
      </Box>
    </ChakraProvider>
  );
};

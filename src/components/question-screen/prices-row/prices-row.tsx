import { Heading, IconButton, Tag } from "@chakra-ui/react";
import { useState } from "react";
import styled from "styled-components";
import { Text } from "@chakra-ui/react";
import { EditIcon } from "@chakra-ui/icons";
import { ConstEditForm } from "./components/cost-edit-form";

interface PricesRowProps {
  savedPrices: string[];
  setNewPrices: (prices: number[]) => void;
}

export const PricesRow = ({ savedPrices, setNewPrices }: PricesRowProps) => {
  const [prices, setPrices] = useState(
    savedPrices.map((item) => parseInt(item)) ?? []
  );
  const [isEditing, setIsEditing] = useState<boolean>(false);

  const onSave = () => {
    setIsEditing(false);
    setNewPrices(prices);
  };

  return (
    <Root>
      <Heading as="h5" size="sm">
        Cost of questions in the current round
      </Heading>
      {isEditing ? (
        <ConstEditForm prices={prices} setPrices={setPrices} onSave={onSave} />
      ) : (
        <PricesWrapper>
          {prices?.map((price, index) => (
            <Tag key={index} size="lg" variant="solid" colorScheme="blue">
              {price}
            </Tag>
          ))}

          <IconButton
            aria-label="Search database"
            icon={<EditIcon />}
            onClick={() => {
              setIsEditing(true);
            }}
          />
        </PricesWrapper>
      )}
    </Root>
  );
};

const Root = styled.div``;

const PricesWrapper = styled.div`
  margin-top: 16px;
  display: flex;
  gap: 16px;
`;

import {
  Button,
  FormControl,
  FormLabel,
  IconButton,
  Input,
  useDisclosure,
} from "@chakra-ui/react";
import { useEffect, useRef, useState } from "react";
import { ModalWindow } from "../../../ui/modal-window/modal-window";
import { SettingsIcon } from "@chakra-ui/icons";

interface AddCategoryProps {
  initialName?: string;
  editCategoryName: (newName: string) => void;
}

export const OpenCategorySettings = ({
  initialName,
  editCategoryName,
}: AddCategoryProps) => {
  const [categoryName, setCategoryName] = useState(initialName);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const initialRef = useRef(null);

  const onApply = () => {
    if (!categoryName) return;

    editCategoryName(categoryName);
    onClose();
  };

  return (
    <>
      <IconButton
        onClick={onOpen}
        aria-label="category settings"
        icon={<SettingsIcon />}
        marginRight={2}
      />

      <ModalWindow
        isOpen={isOpen}
        onClose={onClose}
        onApply={onApply}
        title="Category settings"
        applyButtonText="Save"
        isApplyDisabled={!categoryName}
      >
        <FormControl>
          <FormLabel>Category name</FormLabel>
          <Input
            ref={initialRef}
            placeholder="Category name"
            value={categoryName}
            onChange={(e) => setCategoryName(e.target.value)}
          />
        </FormControl>
      </ModalWindow>
    </>
  );
};

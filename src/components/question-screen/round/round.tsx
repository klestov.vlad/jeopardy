import {
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Heading,
  Stack,
} from "@chakra-ui/react";
import { AlertButton } from "../../../ui";
import { PricesRow } from "../prices-row/prices-row";
import { QuestionItemType, RoundType } from "../../../shared/types";
import { Categories } from "../categories/categories";
import { AddCategory } from "../add-category/add-category";

interface RoundProps {
  index: number;
  roundId: string;
  round: RoundType;
  deleteRound: (id: string) => void;
  setPrices: (roundId: string, prices: number[]) => void;
  addCategory: (roundId: string, categoryName: string) => void;
  deleteCategory: (roundId: string, categoryId: string) => void;
  setQuestion: (
    roundId: string,
    categoryId: string,
    price: string,
    question: QuestionItemType
  ) => void;
  editCategoryName: (
    roundId: string,
    categoryId: string,
    newName: string
  ) => void;
  deleteQuestion: (roundId: string, categoryId: string, price: string) => void;
  setCatInABagStatus: (
    roundId: string,
    categoryId: string,
    price: string,
    status: boolean
  ) => void;
  setAuctionQuestionStatus: (
    roundId: string,
    categoryId: string,
    price: string,
    status: boolean
  ) => void;
}

export const Round = ({
  round,
  index,
  roundId,
  deleteRound,
  setPrices,
  addCategory,
  deleteCategory,
  setQuestion,
  editCategoryName,
  deleteQuestion,
  setCatInABagStatus,
  setAuctionQuestionStatus,
}: RoundProps) => {
  return (
    <AccordionItem>
      <h2>
        <AccordionButton>
          <Box as="span" flex="1" textAlign="left">
            <Heading as="h2" size="md">
              Round {index + 1}
            </Heading>
          </Box>
          <AccordionIcon />
        </AccordionButton>
      </h2>

      <AccordionPanel pb={4}>
        <PricesRow
          savedPrices={round.prices}
          setNewPrices={(prices: number[]) => setPrices(roundId, prices)}
        />

        <Categories
          categoriesPrices={round.prices}
          categories={round?.categories}
          deleteCategory={(categoryId: string) =>
            deleteCategory(roundId, categoryId)
          }
          setQuestion={(
            categoryId: string,
            price: string,
            question: QuestionItemType
          ) => setQuestion(roundId, categoryId, price, question)}
          editCategoryName={(categoryId: string, newName: string) =>
            editCategoryName(roundId, categoryId, newName)
          }
          deleteQuestion={(categoryId: string, price: string) =>
            deleteQuestion(roundId, categoryId, price)
          }
          setCatInABagStatus={(
            categoryId: string,
            price: string,
            status: boolean
          ) => setCatInABagStatus(roundId, categoryId, price, status)}
          setAuctionQuestionStatus={(
            categoryId: string,
            price: string,
            status: boolean
          ) => setAuctionQuestionStatus(roundId, categoryId, price, status)}
        />
        <Stack spacing={4} direction="row" align="center" marginTop={4}>
          <AddCategory
            addCategory={(categoryName: string) =>
              addCategory(roundId, categoryName)
            }
          />

          <AlertButton
            title="Delete round"
            buttonName="Delete round"
            onConfirm={() => deleteRound(roundId)}
          />
        </Stack>
      </AccordionPanel>
    </AccordionItem>
  );
};

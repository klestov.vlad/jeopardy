import { Box, Heading } from "@chakra-ui/react";
import { CurrentRoundCategories } from "../current-round-categories/current-round-categories";
import { GameControlContext } from "../../../providers/game-control-provider";
import { useContext } from "react";

export const QuestionsControl = () => {
  const {
    rounds,
    gameData,
    isQuestionSelected,
    setCurrentQuestionData,
    checkIfCurrentQuestionOpen,
  } = useContext(GameControlContext);

  const currentRoundKey = gameData?.currentRound;
  const currentRound = currentRoundKey ? rounds?.[currentRoundKey] : undefined;
  return (
    <>
      <Box marginTop={4}>
        <Heading size="md">Questions control</Heading>

        {currentRound && (
          <CurrentRoundCategories
            isQuestionSelected={isQuestionSelected}
            categories={currentRound.categories}
            setCurrentQuestionData={(currentCategory, currentPrice) =>
              setCurrentQuestionData(
                currentRoundKey,
                currentCategory,
                currentPrice
              )
            }
            checkIfCurrentQuestionOpen={(currentCategory, currentPrice) =>
              checkIfCurrentQuestionOpen(
                currentRoundKey,
                currentCategory,
                currentPrice
              )
            }
          />
        )}
      </Box>
    </>
  );
};

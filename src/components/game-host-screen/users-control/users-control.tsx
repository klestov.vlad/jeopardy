import { Button, Heading, SimpleGrid } from "@chakra-ui/react";
import { UserCart } from "../user-card/user-card";
import { useContext } from "react";
import { GameControlContext } from "../../../providers/game-control-provider";

export const UserControl = () => {
  const {
    users,
    activeUserId,
    setUserName,
    setUserCount,
    setUserIsReady,
    deleteUser,
    resetActiveUser,
    changeUserCount,
  } = useContext(GameControlContext);

  const onCorrectAnswer = (userId: string, questionBit: number) => {
    if (userId) {
      changeUserCount(userId, questionBit);
    }
  };

  const onIncorrectAnswer = (userId: string, questionBit: number) => {
    if (userId) {
      changeUserCount(userId, -questionBit);
    }
  };

  return (
    <>
      <Heading size="md">users control</Heading>
      <SimpleGrid
        marginTop={4}
        spacing={4}
        templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
        alignItems="start"
      >
        {Object.keys(users || {}).map((id) => (
          <UserCart
            key={id}
            id={id}
            user={users?.[id]}
            setUserName={(name: string) => setUserName(id, name)}
            setUserCount={(count: number) => setUserCount(id, count)}
            setUserIsReady={(isReady: boolean) => setUserIsReady(id, isReady)}
            deleteUser={() => deleteUser(id)}
            isActive={id === activeUserId}
            onCorrectAnswer={(questionBit: number) =>
              onCorrectAnswer(id, questionBit)
            }
            onIncorrectAnswer={(questionBit: number) =>
              onIncorrectAnswer(id, questionBit)
            }
          />
        ))}
      </SimpleGrid>
      <Button
        onClick={resetActiveUser}
        colorScheme="teal"
        variant="solid"
        isDisabled={!activeUserId}
        marginTop={2}
      >
        Reset
      </Button>
    </>
  );
};

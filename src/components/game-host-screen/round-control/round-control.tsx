import { ArrowForwardIcon, ViewIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Heading,
  SimpleGrid,
  useDisclosure,
  Text,
} from "@chakra-ui/react";
import { ModalWindow } from "../../../ui";
import { useContext, useState } from "react";
import { GameControlContext } from "../../../providers/game-control-provider";

export const RoundControl = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [currentRoundNumber, setCurrentRoundNumber] = useState<number>(0);

  const {
    rounds,
    gameData,
    resetSpinTry,
    setCurrentRound,
    resetCurrentRound,
    resetCurrentQuestionData,
    setActiveGameScreen,
  } = useContext(GameControlContext);

  return (
    <>
      <Box marginTop={4}>
        <Heading size="md">Round control</Heading>
        <SimpleGrid
          marginTop={4}
          spacing={4}
          templateColumns="repeat(auto-fill, minmax(150px, 1fr))"
        >
          <Button
            onClick={() => {
              resetCurrentRound();
              resetCurrentQuestionData();
              setActiveGameScreen("PREVIEW_SCREEN");
              resetSpinTry();
            }}
            leftIcon={
              !gameData?.currentRound ? <ViewIcon /> : <ArrowForwardIcon />
            }
            colorScheme="red"
            variant="solid"
            isDisabled={!gameData?.currentRound}
          >
            Reset round
          </Button>

          {Object.keys(rounds || {}).map((id, index) => {
            return (
              <Button
                key={id}
                onClick={() => {
                  setCurrentRoundNumber(index + 1);
                  setActiveGameScreen("PREVIEW_SCREEN");
                  onOpen();
                }}
                leftIcon={
                  id === gameData?.currentRound ? (
                    <ViewIcon />
                  ) : (
                    <ArrowForwardIcon />
                  )
                }
                colorScheme="teal"
                variant="solid"
                isDisabled={id === gameData?.currentRound}
              >
                Round {index + 1}
              </Button>
            );
          })}
        </SimpleGrid>
        <SimpleGrid
          marginTop={4}
          spacing={4}
          templateColumns="repeat(auto-fill, minmax(150px, 1fr))"
        >
          <Button
            onClick={() => {
              gameData?.currentRound
                ? setActiveGameScreen("PREVIEW_SCREEN")
                : setActiveGameScreen("QR_CODE_SCREEN");
            }}
            colorScheme="telegram"
            variant="solid"
            isDisabled={gameData?.activeGameScreen === "PREVIEW_SCREEN"}
          >
            Preview screen
          </Button>
          <Button
            onClick={() => {
              setActiveGameScreen("SHARED_GAME_SCREEN");
            }}
            colorScheme="telegram"
            variant="solid"
            isDisabled={
              gameData?.activeGameScreen === "SHARED_GAME_SCREEN" ||
              !gameData?.currentRound
            }
          >
            Game screen
          </Button>
          <Button
            onClick={() => {
              setActiveGameScreen("USERS_GAME_SCORE");
            }}
            colorScheme="telegram"
            variant="solid"
            isDisabled={gameData?.activeGameScreen === "USERS_GAME_SCORE"}
          >
            Score screen
          </Button>
        </SimpleGrid>
      </Box>
      <ModalWindow
        isOpen={isOpen}
        onClose={onClose}
        title={`Move on to round № ${currentRoundNumber}?`}
        onApply={() => {
          const selectedRoundId = Object.keys(rounds || {})[
            currentRoundNumber - 1
          ];
          setCurrentRound(selectedRoundId);
          onClose();
        }}
        applyButtonText="Move on"
      >
        <Text>
          Are you sure you want to move on to round № {currentRoundNumber}?
        </Text>
      </ModalWindow>
    </>
  );
};

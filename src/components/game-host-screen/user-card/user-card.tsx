import {
  Card,
  CardBody,
  Heading,
  TagLabel,
  Tag,
  Input,
  Box,
  Button,
  Switch,
  IconButton,
  Flex,
  Stack,
} from "@chakra-ui/react";
import { UserType } from "../../../shared/types";
import { useState } from "react";
import { DeleteIcon, EditIcon } from "@chakra-ui/icons";
import { AlertButton } from "../../../ui";

interface UserCartProps {
  id: string;
  user?: UserType;
  isActive: boolean;
  setUserName: (name: string) => void;
  setUserCount: (count: number) => void;
  setUserIsReady: (isReady: boolean) => void;
  deleteUser: () => void;
  onCorrectAnswer: (questionBit: number) => void;
  onIncorrectAnswer: (questionBit: number) => void;
}

export const UserCart = ({
  id,
  user,
  isActive,
  setUserName,
  setUserCount,
  setUserIsReady,
  deleteUser,
  onCorrectAnswer,
  onIncorrectAnswer,
}: UserCartProps) => {
  const [isEditing, setIsEditing] = useState(false);
  return (
    <Card maxW="sm" variant={user?.isReady ? "elevated" : "filled"}>
      <CardBody borderColor={"green"} borderWidth={isActive ? 2 : 0}>
        {isEditing ? (
          <Box gap={2} display="flex" flexDir="column">
            <Input
              placeholder="user name"
              size="md"
              value={user?.name}
              onChange={(e) => {
                setUserName(e.target.value);
              }}
            />
            <Input
              type="number"
              placeholder="user count"
              size="md"
              value={user?.count || 0}
              onChange={(e) => {
                setUserCount(Number(e.target.value));
              }}
            />
            <Switch
              isChecked={user?.isReady || false}
              onChange={(e) => {
                setUserIsReady(e.target.checked);
              }}
            />
            <Flex gap={2}>
              <Button
                width={"100%"}
                onClick={() => {
                  setIsEditing(false);
                }}
              >
                Close
              </Button>
              <AlertButton
                icon={<DeleteIcon />}
                title={"Delete user"}
                onConfirm={deleteUser}
              />
            </Flex>
          </Box>
        ) : (
          <Box gap={2} display="flex" flexDir="column" alignItems="flex-start">
            <Box
              display="flex"
              justifyContent="space-between"
              width={"100%"}
              alignItems={"center"}
            >
              <Heading size="sm">{user?.name} </Heading>
              <IconButton
                aria-label="Search database"
                icon={<EditIcon />}
                onClick={() => {
                  setIsEditing(true);
                }}
              />
            </Box>
            <Tag
              size={"md"}
              borderRadius="full"
              variant="solid"
              colorScheme={user?.isReady ? "green" : "red"}
            >
              <TagLabel>{user?.count || 0}</TagLabel>
            </Tag>
            {user?.auctionBid && (
              <Tag
                size={"md"}
                borderRadius="full"
                variant="outline"
                colorScheme={"purple"}
              >
                bit: <TagLabel>{user?.auctionBid}</TagLabel>
              </Tag>
            )}
            {user?.auctionAnswer && (
              <Tag
                size={"md"}
                borderRadius="full"
                variant="outline"
                colorScheme={"purple"}
              >
                answer: <TagLabel>{user?.auctionAnswer}</TagLabel>
              </Tag>
            )}
            {user?.auctionBid && (
              <Stack direction="row" align="center" marginTop={2}>
                <Button
                  colorScheme="green"
                  onClick={() => onCorrectAnswer(Number(user?.auctionBid) || 0)}
                  size={"sm"}
                >
                  Correct
                </Button>
                <Button
                  colorScheme="red"
                  onClick={() =>
                    onIncorrectAnswer(Number(user?.auctionBid) || 0)
                  }
                  size={"sm"}
                >
                  Incorrect
                </Button>
              </Stack>
            )}
          </Box>
        )}
      </CardBody>
    </Card>
  );
};

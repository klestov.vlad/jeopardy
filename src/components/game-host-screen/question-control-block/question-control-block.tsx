import { Box, Stack } from "@chakra-ui/react";
import { QuestionItemType } from "../../../shared/types";
import { QuestionControlCard } from "../question-control-card/question-control-card";

interface QuestionBlockProps {
  questions: {
    [price: string]: QuestionItemType;
  };
  isQuestionSelected: boolean;
  categoryId?: string;

  setCurrentQuestionData: (currentPrice?: string) => void;
  checkIfCurrentQuestionOpen: (currentPrice?: string) => boolean;
}

export const QuestionControlBlock = ({
  questions,
  isQuestionSelected,
  categoryId,

  setCurrentQuestionData,
  checkIfCurrentQuestionOpen,
}: QuestionBlockProps) => {
  return (
    <Box>
      <Stack
        spacing={4}
        direction="row"
        align="top"
        marginTop={4}
        flexWrap={"wrap"}
      >
        {Object.keys(questions || {}).map((price) => (
          <QuestionControlCard
            key={price}
            question={questions[price]}
            price={price}
            setCurrentQuestionData={() => setCurrentQuestionData(price)}
            isCurrentQuestionOpen={checkIfCurrentQuestionOpen(price)}
            isAnyQuestionSelected={isQuestionSelected}
            categoryId={categoryId}
          />
        ))}
      </Stack>
    </Box>
  );
};

import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Heading,
} from "@chakra-ui/react";

import { CategoryType } from "../../../shared/types";
import { QuestionControlBlock } from "../question-control-block/question-control-block";

interface CurrentRoundCategoriesProps {
  categories?: {
    [categoryKey: string]: CategoryType;
  };
  isQuestionSelected: boolean;

  setCurrentQuestionData: (
    currentCategory?: string,
    currentPrice?: string
  ) => void;

  checkIfCurrentQuestionOpen: (
    currentCategory?: string,
    currentPrice?: string
  ) => boolean;
}

export const CurrentRoundCategories = ({
  categories,
  isQuestionSelected,

  setCurrentQuestionData,
  checkIfCurrentQuestionOpen,
}: CurrentRoundCategoriesProps) => {
  return (
    <Box marginTop={4}>
      <Accordion allowToggle defaultIndex={[0]}>
        {categories &&
          Object.keys(categories || {}).map((categoryKey) => {
            return (
              <AccordionItem key={categoryKey}>
                <AccordionButton>
                  <Box as="span" flex="1" textAlign="left">
                    <Heading as="h5" size="sm">
                      {categories[categoryKey]?.name}
                    </Heading>
                  </Box>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel pb={4}>
                  <QuestionControlBlock
                    questions={categories[categoryKey].questions}
                    isQuestionSelected={isQuestionSelected}
                    setCurrentQuestionData={(price?: string) =>
                      setCurrentQuestionData(categoryKey, price)
                    }
                    checkIfCurrentQuestionOpen={(price?: string) =>
                      checkIfCurrentQuestionOpen(categoryKey, price)
                    }
                    categoryId={categoryKey}
                  />
                </AccordionPanel>
              </AccordionItem>
            );
          })}
      </Accordion>
    </Box>
  );
};

import { Box, Button, Checkbox, Heading, SimpleGrid } from "@chakra-ui/react";
import { useContext } from "react";
import { GameControlContext } from "../../../providers/game-control-provider";

export const SuperGameControl = () => {
  const { gameData, setActiveGameScreen, setIsDemoAvailable } =
    useContext(GameControlContext);

  const isDemoSpinAvailable = gameData?.isDemoSpinAvailable;

  return (
    <Box marginTop={4}>
      <Heading size="md">Super game control</Heading>
      <SimpleGrid
        marginTop={4}
        spacing={4}
        templateColumns="repeat(auto-fill, minmax(150px, 1fr))"
      >
        <Button
          onClick={() => {
            setActiveGameScreen("PREVIEW_SUPER_GAME_SCREEN");
          }}
          colorScheme="telegram"
          variant="solid"
          isDisabled={
            gameData?.activeGameScreen === "PREVIEW_SUPER_GAME_SCREEN"
          }
        >
          Preview SG screen
        </Button>
        <Button
          onClick={() => {
            setActiveGameScreen("SUPER_GAME_SCREEN");
          }}
          colorScheme="telegram"
          variant="solid"
          isDisabled={gameData?.activeGameScreen === "SUPER_GAME_SCREEN"}
        >
          Super Game screen
        </Button>
        <Button
          onClick={() => {
            setActiveGameScreen("SUPER_GAME_ANSWERS");
          }}
          colorScheme="telegram"
          variant="solid"
          isDisabled={gameData?.activeGameScreen === "SUPER_GAME_ANSWERS"}
        >
          SG answers
        </Button>
        <Button
          onClick={() => {
            setActiveGameScreen("FINAL_GAME_SCORE");
          }}
          colorScheme="telegram"
          variant="solid"
          isDisabled={gameData?.activeGameScreen === "FINAL_GAME_SCORE"}
        >
          Final score screen
        </Button>
        <Button
          onClick={() => {
            setActiveGameScreen("SPIN_TO_WIN_SCREEN");
          }}
          colorScheme="telegram"
          variant="solid"
          isDisabled={gameData?.activeGameScreen === "SPIN_TO_WIN_SCREEN"}
        >
          Spin to win
        </Button>
      </SimpleGrid>
      <Checkbox
        marginTop={4}
        size="lg"
        colorScheme="orange"
        isChecked={isDemoSpinAvailable}
        onChange={(e) => setIsDemoAvailable(e.target.checked)}
      >
        Is demo spin available
      </Checkbox>
    </Box>
  );
};

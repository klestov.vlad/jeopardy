import {
  Box,
  Button,
  ButtonGroup,
  CardBody,
  CardFooter,
  Divider,
  Highlight,
  Stack,
  Text,
  Image,
  Card,
  Tag,
  Progress,
  Input,
  SimpleGrid,
} from "@chakra-ui/react";
import { QuestionItemType } from "../../../shared/types";
import { AlertButton } from "../../../ui";
import { useContext, useEffect, useState } from "react";
import { UserBlock } from "./components";
import { GameControlContext } from "../../../providers/game-control-provider";

const timeToAnswerMs = 5000;
const progressInterval = timeToAnswerMs / 100;
let interval: NodeJS.Timeout;

interface QuestionCardProps {
  question: QuestionItemType;
  price: string;
  isCurrentQuestionOpen: boolean;
  isAnyQuestionSelected: boolean;
  categoryId?: string;

  setCurrentQuestionData: () => void;
}

export const QuestionControlCard = ({
  question,
  price,
  isCurrentQuestionOpen,
  isAnyQuestionSelected,
  categoryId,
  setCurrentQuestionData,
}: QuestionCardProps) => {
  const [currentProgress, setCurrentProgress] = useState(0);
  const [auctionBid, setAuctionBid] = useState(Number(price));

  const {
    activeUserId,
    gameData,
    users,
    setQuestionStatus,
    setQuestionShowStatus,
    setQuestionAnswerBan,
    setCurrentUser,
  } = useContext(GameControlContext);

  const onOpenQuestion = () => {
    setCurrentQuestionData();

    if (question.isAuctionQuestion || question.isCatInABag) {
      setQuestionAnswerBan(true);
    }
  };

  useEffect(() => {
    if (!isCurrentQuestionOpen) return;

    interval = setInterval(() => {
      setCurrentProgress((prev) => {
        if (prev >= 100) {
          clearInterval(interval);
          return 0;
        }
        return prev + 1;
      });
    }, progressInterval);
    return () => {
      clearInterval(interval);
      setCurrentProgress(0);
    };
  }, [isCurrentQuestionOpen]);

  return (
    <Card
      maxW="md"
      minW="md"
      bg={
        isCurrentQuestionOpen
          ? "green.100"
          : question.isAnswered
          ? "gray.100"
          : "white"
      }
    >
      <CardBody opacity={question.isAnswered ? 0.3 : 1}>
        <Box marginBottom={4}>
          <Tag size="lg" variant="solid" colorScheme="blue">
            {price}
          </Tag>
        </Box>
        {question.question.url && (
          <Image
            src={question.question.url}
            alt="Green double couch with wooden legs"
            borderRadius="lg"
          />
        )}
        <Stack mt="6" spacing="3">
          <Text>
            <Highlight
              query="Q:"
              styles={{ px: "2", py: "1", rounded: "full", bg: "blue.100" }}
            >
              {`${"Q: "}` + question.question.text}
            </Highlight>
          </Text>
          {question.answer.url && (
            <Image
              src={question.answer.url}
              alt="Green double couch with wooden legs"
              borderRadius="lg"
            />
          )}
          <Text>
            <Highlight
              query="A:"
              styles={{ px: "2", py: "1", rounded: "full", bg: "green.100" }}
            >
              {`${"A: "}` + question.answer.text}
            </Highlight>
          </Text>
          <Box
            marginTop={4}
            display={"flex"}
            flexDirection={"column"}
            gap={4}
            alignItems={"flex-start"}
          >
            {question.isCatInABag && (
              <>
                <Tag size="lg" variant="solid" colorScheme="teal">
                  Cat in the bag
                </Tag>
                {isCurrentQuestionOpen && (
                  <SimpleGrid columns={3} spacing={4}>
                    {Object.keys(users || {}).map((userId) => (
                      <Button
                        key={userId}
                        colorScheme={activeUserId === userId ? "green" : "blue"}
                        onClick={() => setCurrentUser(userId)}
                      >
                        {users?.[userId].name}
                      </Button>
                    ))}
                  </SimpleGrid>
                )}
                <Button
                  colorScheme="blue"
                  onClick={() => setQuestionShowStatus(true)}
                  isDisabled={
                    gameData?.isQuestionShow ||
                    !isCurrentQuestionOpen ||
                    !activeUserId
                  }
                >
                  show question
                </Button>
              </>
            )}
            {question.isAuctionQuestion && (
              <>
                <Tag size="lg" variant="solid" colorScheme="purple">
                  Auction question
                </Tag>
                {isCurrentQuestionOpen && (
                  <>
                    <SimpleGrid columns={3} spacing={4}>
                      {Object.keys(users || {}).map((userId) => (
                        <Button
                          key={userId}
                          colorScheme={
                            activeUserId === userId ? "green" : "blue"
                          }
                          onClick={() => setCurrentUser(userId)}
                        >
                          {users?.[userId].name}
                        </Button>
                      ))}
                    </SimpleGrid>
                    <Input
                      type="number"
                      placeholder="user count"
                      size="md"
                      value={auctionBid}
                      onChange={(e) => setAuctionBid(Number(e.target.value))}
                    />
                  </>
                )}
                <Button
                  colorScheme="blue"
                  onClick={() => {
                    setQuestionShowStatus(true);
                  }}
                  isDisabled={
                    gameData?.isQuestionShow ||
                    !isCurrentQuestionOpen ||
                    !auctionBid ||
                    !activeUserId
                  }
                >
                  show question
                </Button>
              </>
            )}
          </Box>
        </Stack>
      </CardBody>
      {isCurrentQuestionOpen && (
        <UserBlock
          questionPrice={
            question.isAuctionQuestion ? auctionBid : Number(price)
          }
        />
      )}
      <Divider />
      {Boolean(currentProgress) && (
        <Progress hasStripe value={currentProgress} />
      )}
      <CardFooter width="100%" display="flex">
        <ButtonGroup width="100%" justifyContent="space-between" display="flex">
          <Button
            colorScheme="blue"
            onClick={onOpenQuestion}
            isDisabled={isAnyQuestionSelected || question?.isAnswered}
          >
            Open question
          </Button>
          {question?.isAnswered && (
            <AlertButton
              title="Return question"
              buttonName="Return"
              description="Are you sure you want to return this question?"
              onConfirm={() =>
                setQuestionStatus(
                  false,
                  gameData?.currentRound,
                  categoryId,
                  price
                )
              }
              cancelButtonText="Cancel"
              confirmButtonText="Return"
              variant="ghost"
            />
          )}
        </ButtonGroup>
      </CardFooter>
    </Card>
  );
};

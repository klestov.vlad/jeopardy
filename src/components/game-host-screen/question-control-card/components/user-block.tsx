import { Badge, Box, Button, Divider, Stack, Text } from "@chakra-ui/react";
import { useContext } from "react";
import { GameControlContext } from "../../../../providers/game-control-provider";

interface UserBlockProps {
  questionPrice: number;
}

export const UserBlock = ({ questionPrice }: UserBlockProps) => {
  const {
    activeUserId,
    users,
    gameData,
    changeUserCount,
    resetActiveUser,
    resetCurrentQuestionData,
    setQuestionStatus,
    setQuestionShowStatus,
    setQuestionAnswerBan,
  } = useContext(GameControlContext);

  const user = activeUserId ? users?.[activeUserId] : undefined;

  const makeQuestionAnsweredAndResetQuestion = () => {
    setQuestionStatus(
      true,
      gameData?.currentRound,
      gameData?.currentCategory,
      gameData?.currentPrice
    );
    resetActiveUser();
    resetCurrentQuestionData();
    setQuestionShowStatus(false);
    setQuestionAnswerBan(false);
  };

  const onCorrectAnswer = () => {
    if (activeUserId) {
      changeUserCount(activeUserId, questionPrice);
      makeQuestionAnsweredAndResetQuestion();
    }
  };

  const onIncorrectAnswer = () => {
    if (activeUserId) {
      changeUserCount(activeUserId, -questionPrice);
      makeQuestionAnsweredAndResetQuestion();
    }
  };

  return Boolean(user) ? (
    <Box backgroundColor={"gray.100"} padding={4}>
      <Divider />
      <Text>
        Answers user <Badge colorScheme="purple">{user?.name}</Badge>
      </Text>
      <Stack direction="row" spacing={4} align="center" marginTop={2}>
        <Button colorScheme="green" onClick={onCorrectAnswer}>
          Correct
        </Button>
        <Button colorScheme="red" onClick={onIncorrectAnswer}>
          Incorrect
        </Button>
      </Stack>
    </Box>
  ) : (
    <Box backgroundColor={"gray.100"} padding={4}>
      <Button colorScheme="red" onClick={makeQuestionAnsweredAndResetQuestion}>
        Close question
      </Button>
    </Box>
  );
};

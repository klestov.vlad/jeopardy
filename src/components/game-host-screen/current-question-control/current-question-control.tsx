import { Box, Button, Heading, Tag, TagLabel, Text } from "@chakra-ui/react";
import { useContext } from "react";
import { GameControlContext } from "../../../providers/game-control-provider";

export const CurrentQuestionControl = () => {
  const { currentQuestionData, resetCurrentQuestionData } =
    useContext(GameControlContext);

  const isQuestionExists = Boolean(currentQuestionData?.currentPrice);

  return (
    <Box marginTop={4}>
      <Heading size="md">Round control question</Heading>
      {isQuestionExists ? (
        <Box marginTop={4} display={"flex"} gap={4}>
          <Tag
            borderRadius="full"
            variant="solid"
            colorScheme="green"
            alignItems={"center"}
            justifyContent={"center"}
            display={"flex"}
          >
            <TagLabel>{currentQuestionData?.currentCategory}</TagLabel>
          </Tag>

          <Tag
            borderRadius="full"
            variant="solid"
            colorScheme="green"
            alignItems={"center"}
            justifyContent={"center"}
            display={"flex"}
          >
            <TagLabel>{currentQuestionData?.currentPrice}</TagLabel>
          </Tag>

          <Button
            onClick={resetCurrentQuestionData}
            colorScheme="teal"
            variant="solid"
          >
            Reset
          </Button>
        </Box>
      ) : (
        <Box marginTop={4} height={"40px"}>
          <Text>Question not selected</Text>
        </Box>
      )}
    </Box>
  );
};

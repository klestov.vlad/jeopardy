import { Box, Heading, Input, InputGroup, SimpleGrid } from "@chakra-ui/react";
import { useContext } from "react";
import { GameControlContext } from "../../../providers/game-control-provider";

export const SharedUrlControl = () => {
  const { setSharedUrl, gameData } = useContext(GameControlContext);

  return (
    <Box marginTop={4}>
      <Heading size="md">shared url control</Heading>
      <SimpleGrid marginTop={4} spacing={4} columns={2}>
        <Box>
          <InputGroup>
            <Input
              type="url"
              id="url"
              placeholder="Please enter wi-fi ip-address"
              value={gameData?.url}
              onChange={(event) => setSharedUrl(event.target.value)}
            />
          </InputGroup>
        </Box>
      </SimpleGrid>
    </Box>
  );
};

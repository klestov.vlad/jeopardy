import { createContext } from "react";
import { useHostGameControl } from "../shared/hooks/useHostGame";
import { GameDataType, QuestionType, UsersType } from "../shared/types";
import { ActiveGameScreens } from "../shared/constants/routes";

export const GameControlContext = createContext<{
  activeUserId?: string;
  gameData?: GameDataType;
  rounds?: QuestionType;
  users?: UsersType;
  isQuestionSelected: boolean;
  currentQuestionData: {
    currentCategory?: string;
    currentPrice?: string;
  };
  setUserName: (userId: string, name: string) => void;
  setUserCount: (userId: string, count: number) => void;
  setUserIsReady: (userId: string, isReady: boolean) => void;
  deleteUser: (userId: string) => void;
  setCurrentRound: (round: string) => void;
  setCurrentQuestionData: (
    currentRound?: string | undefined,
    currentCategory?: string | undefined,
    currentPrice?: string | undefined
  ) => void;
  resetCurrentQuestionData: () => void;
  checkIfCurrentQuestionOpen: (
    currentRound?: string | undefined,
    currentCategory?: string | undefined,
    currentPrice?: string | undefined
  ) => boolean;
  resetActiveUser: () => void;
  changeUserCount: (userId: string, changeCount: number) => void;
  setQuestionStatus: (
    isAnswered: boolean,
    currentRoundId?: string | undefined,
    currentCategoryId?: string | undefined,
    currentPrice?: string | undefined
  ) => void;
  setSharedUrl: (url: string) => void;
  resetCurrentRound: () => void;
  setActiveGameScreen: (screen: keyof typeof ActiveGameScreens) => void;
  setQuestionShowStatus: (showQuestion: boolean) => void;
  setQuestionAnswerBan: (isAnswerBan: boolean) => void;
  setCurrentUser: (userId: string) => void;
  setIsDemoAvailable: (isAvailable: boolean) => void;
  resetSpinTry: () => void;
}>({
  isQuestionSelected: false,
  currentQuestionData: {},
  setUserName: () => null,
  setUserCount: () => null,
  setUserIsReady: () => null,
  deleteUser: () => null,
  setCurrentRound: () => null,
  setCurrentQuestionData: () => null,
  resetCurrentQuestionData: () => null,
  checkIfCurrentQuestionOpen: () => false,
  resetActiveUser: () => null,
  changeUserCount: () => null,
  setQuestionStatus: () => null,
  setSharedUrl: () => null,
  resetCurrentRound: () => null,
  setActiveGameScreen: () => null,
  setQuestionShowStatus: () => null,
  setQuestionAnswerBan: () => null,
  setCurrentUser: () => null,
  setIsDemoAvailable: (isAvailable: boolean) => null,
  resetSpinTry: () => null,
});

interface GameControlProviderProps {
  children: React.ReactNode;
}

export const GameControlProvider = ({ children }: GameControlProviderProps) => {
  const {
    activeUserId,
    gameData,
    rounds,
    users,
    isQuestionSelected,
    currentQuestionData,
    setUserName,
    setUserCount,
    setUserIsReady,
    deleteUser,
    setCurrentRound,
    setCurrentQuestionData,
    resetCurrentQuestionData,
    checkIfCurrentQuestionOpen,
    resetActiveUser,
    changeUserCount,
    setQuestionStatus,
    setSharedUrl,
    resetCurrentRound,
    setActiveGameScreen,
    setQuestionShowStatus,
    setQuestionAnswerBan,
    setCurrentUser,
    setIsDemoAvailable,
    resetSpinTry,
  } = useHostGameControl();

  window.addEventListener("keydown", function (event) {
    if (event.code === "Space") {
      event.preventDefault();
      resetCurrentQuestionData();
      resetActiveUser();
      setQuestionShowStatus(false);
      setQuestionAnswerBan(false);
    }
  });

  return (
    <GameControlContext.Provider
      value={{
        activeUserId,
        gameData,
        rounds,
        users,
        isQuestionSelected,
        currentQuestionData,
        setUserName,
        setUserCount,
        setUserIsReady,
        deleteUser,
        setCurrentRound,
        setCurrentQuestionData,
        resetCurrentQuestionData,
        checkIfCurrentQuestionOpen,
        resetActiveUser,
        changeUserCount,
        setQuestionStatus,
        setSharedUrl,
        resetCurrentRound,
        setActiveGameScreen,
        setQuestionShowStatus,
        setQuestionAnswerBan,
        setCurrentUser,
        setIsDemoAvailable,
        resetSpinTry,
      }}
    >
      {children}
    </GameControlContext.Provider>
  );
};

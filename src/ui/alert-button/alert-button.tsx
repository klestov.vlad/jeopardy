import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Button,
  IconButton,
  ResponsiveValue,
  useDisclosure,
} from "@chakra-ui/react";
import { ReactElement, useRef } from "react";

interface AlertButtonProps {
  title: string;
  icon?: ReactElement<any, string | React.JSXElementConstructor<any>>;
  buttonName?: string;
  description?: string;
  confirmButtonText?: string;
  cancelButtonText?: string;
  onConfirm: () => void;
  variant?:
    | ResponsiveValue<
        "link" | "outline" | (string & {}) | "ghost" | "solid" | "unstyled"
      >
    | undefined;
}

export const AlertButton = ({
  title,
  icon,
  buttonName,
  description = `Are you sure? You can't undo this action afterwards.`,
  confirmButtonText = "Confirm",
  cancelButtonText = "Cancel",
  variant,
  onConfirm,
}: AlertButtonProps) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = useRef<HTMLButtonElement>(null);

  const onConfirmClick = async () => {
    await onConfirm();
    onClose();
  };

  return (
    <>
      {icon ? (
        <IconButton
          colorScheme="red"
          onClick={onOpen}
          aria-label="remove-field"
          icon={icon}
        />
      ) : (
        <Button colorScheme="red" onClick={onOpen} variant={variant}>
          {buttonName}
        </Button>
      )}

      <AlertDialog
        isOpen={isOpen}
        leastDestructiveRef={cancelRef}
        onClose={onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              {title}
            </AlertDialogHeader>

            <AlertDialogBody>{description}</AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={onClose}>
                {cancelButtonText}
              </Button>
              <Button colorScheme="red" onClick={onConfirmClick} ml={3}>
                {confirmButtonText}
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  );
};

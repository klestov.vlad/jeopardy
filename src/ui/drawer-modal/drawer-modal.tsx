import {
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  Stack,
} from "@chakra-ui/react";

interface DrawerModalProps {
  isOpen: boolean;
  onClose: () => void;

  title?: string;
  children: React.ReactNode;
  onApply?: () => void;
  applyButtonText?: string;
  isApplyDisabled?: boolean;
}

export const DrawerModal = ({
  isOpen,
  title,
  children,
  onClose,
  onApply,
  applyButtonText = "Apply",
  isApplyDisabled = false,
}: DrawerModalProps) => {
  return (
    <Drawer isOpen={isOpen} placement="right" onClose={onClose} size="md">
      <DrawerOverlay />
      <DrawerContent>
        <DrawerCloseButton />
        <DrawerHeader borderBottomWidth="1px">{title}</DrawerHeader>

        <DrawerBody>
          <Stack spacing="24px">{children}</Stack>
        </DrawerBody>

        {Boolean(onApply) && (
          <DrawerFooter borderTopWidth="1px">
            <Button variant="outline" mr={3} onClick={onClose}>
              Cancel
            </Button>
            <Button
              onClick={onApply}
              colorScheme="blue"
              isDisabled={isApplyDisabled}
            >
              {applyButtonText}
            </Button>
          </DrawerFooter>
        )}
      </DrawerContent>
    </Drawer>
  );
};

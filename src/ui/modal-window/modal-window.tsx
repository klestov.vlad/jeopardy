import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react";
import { useRef } from "react";

interface ModalWindowProps {
  isOpen: boolean;
  onClose: () => void;

  title?: string;
  children: React.ReactNode;
  onApply?: () => void;
  applyButtonText?: string;
  isApplyDisabled?: boolean;
}

export const ModalWindow = ({
  isOpen,
  title,
  children,
  isApplyDisabled,
  onClose,
  onApply,
  applyButtonText = "Apply",
}: ModalWindowProps) => {
  const initialRef = useRef(null);

  return (
    <Modal initialFocusRef={initialRef} isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        {title && <ModalHeader>{title}</ModalHeader>}
        <ModalCloseButton />
        <ModalBody pb={6}>{children}</ModalBody>

        {Boolean(onApply) && (
          <ModalFooter>
            <Button
              isDisabled={isApplyDisabled}
              colorScheme="blue"
              mr={3}
              onClick={onApply}
            >
              {applyButtonText}
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        )}
      </ModalContent>
    </Modal>
  );
};

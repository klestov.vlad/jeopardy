import React from "react";
import "./App.css";
import { RootRoutes } from "./processes/routing/root-routes";
import { ChakraProvider } from "@chakra-ui/react";

function App() {
  return (
    <ChakraProvider>
      <RootRoutes />
    </ChakraProvider>
  );
}

export default App;

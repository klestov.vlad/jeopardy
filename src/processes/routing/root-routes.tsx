import { BrowserRouter, Route, Routes } from "react-router-dom";
import {
  WelcomeScreen,
  IntroduceScreen,
  GameScreen,
  SharedGameScreen,
  QrCodeScreen,
} from "../../pages";
import { QuestionScreen } from "../../pages/question-screen/question-screen";
import { ROUTES } from "../../shared/constants/routes";
import { GameHostScreen } from "../../pages/game-host-screen/game-host-screen";
import { GamePreviewScreen } from "../../pages/shared-preview-screen/shared-preview-screen";
import { UsersGameScore } from "../../pages/users-game-score/users-game-score";
import { ComingSoonScreen } from "../../pages/coming-soon-screen/coming-soon-screen";
import { ReadyToPlayScreen } from "../../pages/ready-to-play-screen.tsx/ready-to-play-screen.tsx";
import { SuperGameScreen } from "../../pages/super-game-screen/super-game-screen";
import { SuperGamePreviewScreen } from "../../pages/super-game-preview-screen/super-game-preview-screen";
import { FinalGameScore } from "../../pages/final-game-score/final-game-score";
import { SuperGameAnswers } from "../../pages/super-game-answers/super-game-answers";
import { SpinToWin } from "../../pages/spin-to-win/spin-to-win";

export const RootRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={ROUTES.HOME} element={<WelcomeScreen />} />
        <Route path={ROUTES.INTRODUCE_SCREEN} element={<IntroduceScreen />} />
        <Route path={ROUTES.GAME_SCREEN} element={<GameScreen />} />
        <Route path={ROUTES.QUESTION_SCREEN} element={<QuestionScreen />} />
        <Route
          path={ROUTES.READY_TO_PLAY_SCREEN}
          element={<ReadyToPlayScreen />}
        />
        <Route
          path={ROUTES.SHARED_GAME_SCREEN}
          element={<SharedGameScreen />}
        />
        <Route path={ROUTES.USERS_GAME_SCORE} element={<UsersGameScore />} />
        <Route path={ROUTES.PREVIEW_SCREEN} element={<GamePreviewScreen />} />
        <Route path={ROUTES.GAME_HOST_SCREEN} element={<GameHostScreen />} />
        <Route path={ROUTES.QR_CODE_SCREEN} element={<QrCodeScreen />} />
        <Route
          path={ROUTES.COMING_SOON_SCREEN}
          element={<ComingSoonScreen />}
        />
        <Route path={ROUTES.SUPER_GAME_SCREEN} element={<SuperGameScreen />} />
        <Route
          path={ROUTES.PREVIEW_SUPER_GAME_SCREEN}
          element={<SuperGamePreviewScreen />}
        />
        <Route path={ROUTES.FINAL_GAME_SCORE} element={<FinalGameScore />} />
        <Route
          path={ROUTES.SUPER_GAME_ANSWERS}
          element={<SuperGameAnswers />}
        />
        <Route path={ROUTES.SPIN_TO_WIN_SCREEN} element={<SpinToWin />} />
        <Route path="*" element={<div>404</div>} />
      </Routes>
    </BrowserRouter>
  );
};

import styled from "styled-components";
import { Heading } from "@chakra-ui/react";

interface MainLayoutProps {
  title?: string;
  children: React.ReactNode;
}

export const MainLayout = ({ children, title }: MainLayoutProps) => {
  return (
    <Wrapper>
      {title && (
        <Heading as="h1" size="xl">
          {title}
        </Heading>
      )}
      <ChildrenWrapper>{children}</ChildrenWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  padding: 20px;
`;

const ChildrenWrapper = styled.div`
  margin-top: 20px;
`;

import styled from "styled-components";

interface GamerLayoutProps {
  children: React.ReactNode;
}

export const GamerLayout = ({ children }: GamerLayoutProps) => {
  return <Wrapper>{children}</Wrapper>;
};

const Wrapper = styled.div`
  position: relative;
  margin: 0 auto;
  width: 100vw;
  max-width: 500px;
  height: 100svh;
  background: white;
  padding: 0 30px;
  overflow: hidden;
`;

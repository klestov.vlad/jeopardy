import { ActiveGameScreens } from "../constants/routes";

export type QuestionItemType = {
  question: {
    text: string;
    url?: string;
  };
  answer: {
    text: string;
    url?: string;
  };
  isAnswered?: boolean;
  isCatInABag?: boolean;
  isAuctionQuestion?: boolean;
};

export type SuperGameType = {
  theme?: string;
  question?: string;
  answer?: string;
};

export type CategoryType = {
  name: string;
  questions: {
    [price: string]: QuestionItemType;
  };
};

export type RoundType = {
  prices: string[];
  categories?: {
    [categoryKey: string]: CategoryType;
  };
};

export type QuestionType = {
  [roundKey: string]: RoundType;
};

export type UserType = {
  name: string;
  isReady?: boolean;
  count?: number;
  auctionBid?: number;
  auctionAnswer?: string;
};

export type UsersType = {
  [userKey: string]: UserType;
};

export type GameDataType = {
  currentCategory: string;
  currentPrice?: string;
  currentRound: string;
  url?: string;
  activeGameScreen?: keyof typeof ActiveGameScreens;
  isQuestionShow?: boolean;
  isAnswerBan?: boolean;
  isSpinStarted?: boolean;
  isFirstTimeSpinAvailable?: boolean;
  isDemoSpinAvailable?: boolean;
};

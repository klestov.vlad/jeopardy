class StorageService {
  public myAuthorizationId = "@jeopardy-my-authorization-id";

  setMyAuthorizationId = (id: string) => {
    localStorage.setItem(this.myAuthorizationId, id);
  };

  getMyAuthorizationId = () => {
    return localStorage.getItem(this.myAuthorizationId);
  };

  removeMyAuthorizationId = () => {
    localStorage.removeItem(this.myAuthorizationId);
  };
}

// eslint-disable-next-line import/no-anonymous-default-export
export default new StorageService();

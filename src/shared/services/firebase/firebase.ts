import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyD1EsE7ROKZkprXgLN5jkY__8XtYpZhv2Y",
  authDomain: "jeopardy-game-b6ae9.firebaseapp.com",
  databaseURL:
    "https://jeopardy-game-b6ae9-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "jeopardy-game-b6ae9",
  storageBucket: "jeopardy-game-b6ae9.appspot.com",
  messagingSenderId: "701113752498",
  appId: "1:701113752498:web:ffbabcfc8dbaceaa99a2ab",
};

const app = initializeApp(firebaseConfig);

export const database = getDatabase(app);

import { onValue, ref } from "firebase/database";
import { database } from "../services/firebase/firebase";
import { FirebasePath } from "../constants/firebase";
import { useEffect, useState } from "react";
import { GameDataType } from "../types";

export const useWelcomeScreenInfo = () => {
  const [gameData, setGameData] = useState<GameDataType>();

  const gameDataRef = ref(database, FirebasePath.GAME_DATA);

  useEffect(() => {
    onValue(gameDataRef, (snapshot) => {
      const data = snapshot.val();

      setGameData(data || {});
    });
  }, []);

  const sharedUrl = gameData?.url;
  const isGameStarted = Boolean(gameData?.currentRound);

  return {
    sharedUrl,
    isGameStarted,
  };
};

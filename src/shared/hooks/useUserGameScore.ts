import { onValue, ref } from "firebase/database";
import { database } from "../services/firebase/firebase";
import { FirebasePath } from "../constants/firebase";
import { useEffect, useState } from "react";

import { UsersType } from "../types";

export const useUserGameScore = () => {
  const [users, setUsers] = useState<UsersType>();

  const usersRef = ref(database, FirebasePath.USERS);

  useEffect(() => {
    onValue(usersRef, (snapshot) => {
      const data = snapshot.val();
      setUsers(data);
    });
  }, []);

  return {
    users: users || {},
  };
};

import { onValue, get, ref } from "firebase/database";
import { database } from "../services/firebase/firebase";
import { useEffect } from "react";
import { FirebasePath } from "../constants/firebase";

export const useFirebaseConnections = () => {
  const dbRef = ref(database, FirebasePath.CATEGORIES);

  useEffect(() => {
    onValue(dbRef, (snapshot) => {
      const data = snapshot.val();
    });
  }, [dbRef]);
};

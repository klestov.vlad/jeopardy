import { child, onValue, ref, set } from "firebase/database";
import { database } from "../services/firebase/firebase";
import { FirebasePath } from "../constants/firebase";
import { useEffect, useState } from "react";

import { GameDataType, UsersType } from "../types";

export const useUserSpinToWin = () => {
  const [users, setUsers] = useState<UsersType>();
  const [gameData, setGameData] = useState<GameDataType>();

  const usersRef = ref(database, FirebasePath.USERS);
  const gameDataRef = ref(database, FirebasePath.GAME_DATA);

  useEffect(() => {
    onValue(usersRef, (snapshot) => {
      const data = snapshot.val();
      setUsers(data);
    });

    onValue(gameDataRef, (snapshot) => {
      const data = snapshot.val();

      setGameData(data || {});
    });
  }, []);

  const setSpinTry = (state: boolean) => {
    const isSpinStartedRef = child(gameDataRef, FirebasePath.IS_SPIN_STARTED);
    const isFirstTimeSpinAvailableRef = child(
      gameDataRef,
      FirebasePath.IS_FIRST_TIME_SPIN_AVAILABLE
    );

    set(isSpinStartedRef, state);
    set(isFirstTimeSpinAvailableRef, false);
  };

  const isDemoSpinAvailable = gameData?.isDemoSpinAvailable;
  const isSpinStarted = gameData?.isSpinStarted;
  const isFirstTimeSpinAvailable = gameData?.isFirstTimeSpinAvailable;

  return {
    users: users || {},
    isFirstTimeSpinAvailable,
    isDemoSpinAvailable,
    isSpinStarted,
    setSpinTry,
  };
};

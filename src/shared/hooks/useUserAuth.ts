import { child, onValue, ref, set } from "firebase/database";
import { database } from "../services/firebase/firebase";
import { FirebasePath } from "../constants/firebase";
import { useEffect, useState } from "react";
import storageService from "../lib/utils/storage-service";
import { UserType } from "../types";
import { v4 as uuid } from "uuid";

const initialRound = {
  prices: [100, 200, 300, 400, 500],
};

export const useUserAuth = () => {
  const [myId, setMyId] = useState(() => storageService.getMyAuthorizationId());
  const [user, setUser] = useState<UserType>();
  const usersRef = ref(database, FirebasePath.USERS);

  useEffect(() => {
    if (storageService.getMyAuthorizationId()) return;
    const newId = uuid();
    setMyId(newId);
    storageService.setMyAuthorizationId(newId);
  }, []);

  const setUserName = (userName: string) => {
    const path = child(usersRef, `${myId}/name`);
    set(path, userName);
  };

  const setUserIsReady = (isReady: boolean) => {
    const path = child(usersRef, `${myId}/isReady`);
    set(path, isReady);
  };

  useEffect(() => {
    if (!myId) return;

    const userRef = child(usersRef, myId);

    onValue(userRef, (snapshot) => {
      const data = snapshot.val();

      setUser(data || {});
    });
  }, [myId]);

  return {
    user,
    setUserName,
    setUserIsReady,
  };
};

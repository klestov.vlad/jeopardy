import { child, onValue, ref } from "firebase/database";
import { database } from "../services/firebase/firebase";
import { FirebasePath } from "../constants/firebase";
import { useEffect, useState } from "react";
import { GameDataType, QuestionItemType, RoundType } from "../types";

export const useSharedGame = () => {
  const [questions, setQuestions] = useState<RoundType>();
  const [gameData, setGameData] = useState<GameDataType>();
  const [currentQuestion, setCurrentQuestion] = useState<QuestionItemType>();

  const questionRef = ref(database, FirebasePath.QUESTIONS);
  const gameDataRef = ref(database, FirebasePath.GAME_DATA);

  useEffect(() => {
    onValue(gameDataRef, (snapshot) => {
      const data = snapshot.val();

      setGameData(data || {});
    });
  }, []);

  useEffect(() => {
    const currentRound = gameData?.currentRound;
    if (!currentRound) {
      return;
    }

    const currentRoundRef = child(questionRef, currentRound);

    onValue(currentRoundRef, (snapshot) => {
      const data = snapshot.val();

      setQuestions(data || {});
    });
  }, [gameData]);

  useEffect(() => {
    if (gameData && questions) {
      const currentRound = gameData.currentRound;
      const currentCategory = gameData.currentCategory;
      const currentPrice = gameData.currentPrice;

      if (!currentRound || !currentCategory || !currentPrice) {
        setCurrentQuestion(undefined);
        return;
      }

      const currentQuestion =
        questions?.categories?.[currentCategory]?.questions?.[currentPrice];

      setCurrentQuestion(currentQuestion);
    }
  }, [gameData, questions]);

  return {
    gameData,
    currentQuestion,
    questions,
  };
};

import { child, onValue, ref, set } from "firebase/database";
import { database } from "../services/firebase/firebase";
import { FirebasePath } from "../constants/firebase";
import { useEffect, useState } from "react";
import storageService from "../lib/utils/storage-service";
import { GameDataType, UserType, UsersType } from "../types";

export const useUserGameControl = () => {
  const myId = storageService.getMyAuthorizationId();

  const [user, setUser] = useState<UserType>();
  const [activeUserId, setActiveUserId] = useState<string>();
  const [gameData, setGameData] = useState<GameDataType>();

  const [users, setUsers] = useState<UsersType>();

  const usersRef = ref(database, FirebasePath.USERS);
  const activeUserRef = ref(database, FirebasePath.ACTIVE_USER);
  const gameDataRef = ref(database, FirebasePath.GAME_DATA);

  const isAnswerBan = Boolean(gameData?.isAnswerBan);

  const isQuestionSelected = Boolean(
    gameData?.currentRound &&
      gameData?.currentCategory &&
      gameData?.currentPrice
  );

  const setActiveUser = () => {
    if (!myId) return;
    if (!isQuestionSelected) return;
    if (activeUserId) return;
    if (isAnswerBan) return;

    set(activeUserRef, myId);
  };

  const setUserBet = (bet: string) => {
    if (!myId) return;
    const userRef = child(usersRef, myId);
    const betRef = child(userRef, FirebasePath.USER_AUCTION_BID);

    set(betRef, Number(bet));
  };

  const setUserAnswer = (answer: string) => {
    if (!myId) return;
    const userRef = child(usersRef, myId);
    const answerRef = child(userRef, FirebasePath.USER_SUPER_GAME_ANSWER);

    set(answerRef, answer);
  };

  const isFirstTimeSpinAvailable = gameData?.isFirstTimeSpinAvailable;
  const isDemoSpinAvailable = gameData?.isDemoSpinAvailable;
  const isSpinStarted = gameData?.isSpinStarted;

  const setSpinTry = () => {
    if (!isFirstTimeSpinAvailable && !isDemoSpinAvailable) return;

    const isFirstTimeSpinAvailableRef = child(
      gameDataRef,
      FirebasePath.IS_FIRST_TIME_SPIN_AVAILABLE
    );

    const isSpinStartedRef = child(gameDataRef, FirebasePath.IS_SPIN_STARTED);

    set(isFirstTimeSpinAvailableRef, true);
    set(isSpinStartedRef, true);
  };

  useEffect(() => {
    onValue(activeUserRef, (snapshot) => {
      const data = snapshot.val();
      setActiveUserId(data);
    });

    onValue(gameDataRef, (snapshot) => {
      const data = snapshot.val();
      setGameData(data);
    });

    onValue(usersRef, (snapshot) => {
      const data = snapshot.val();
      setUsers(data);
    });
  }, []);

  useEffect(() => {
    if (!myId) return;

    const userRef = child(usersRef, myId);

    onValue(userRef, (snapshot) => {
      const data = snapshot.val();

      setUser(data || {});
    });
  }, [myId]);

  const usersByScore = Object.keys(users || {}).sort((a, b) => {
    return Number(users?.[b]?.count || "0") - Number(users?.[a]?.count || "0");
  });

  const isWinnerIsMe = Boolean(usersByScore[0] === myId);

  const isMyTurn = activeUserId === myId;
  const otherUserTurn = Boolean(activeUserId) && activeUserId !== myId;
  const isSuperGame = Boolean(
    gameData?.activeGameScreen === "SUPER_GAME_SCREEN"
  );

  const isAuctionBit = Boolean(
    gameData?.activeGameScreen === "PREVIEW_SUPER_GAME_SCREEN"
  );

  const isSpinToWin = Boolean(
    gameData?.activeGameScreen === "SPIN_TO_WIN_SCREEN"
  );

  return {
    user,
    isMyTurn,
    otherUserTurn,
    activeUserId,
    isSuperGame,
    isAuctionBit,
    isSpinToWin,
    isWinnerIsMe,
    isFirstTimeSpinAvailable,
    isDemoSpinAvailable,
    isSpinStarted,
    setActiveUser,
    setUserBet,
    setUserAnswer,
    setSpinTry,
  };
};

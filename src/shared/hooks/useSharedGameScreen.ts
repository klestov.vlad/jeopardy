import { onValue, ref } from "firebase/database";
import { database } from "../services/firebase/firebase";
import { FirebasePath } from "../constants/firebase";
import { useEffect, useState } from "react";

import { GameDataType, SuperGameType } from "../types";
import { ActiveGameScreens } from "../constants/routes";

export const useSharedGameScreen = () => {
  const [gameData, setGameData] = useState<GameDataType>();
  const [superGameData, setSuperGameData] = useState<SuperGameType>();
  const [categories, setCategories] = useState<string[]>();

  const gameDataRef = ref(database, FirebasePath.GAME_DATA);
  const superGameDataRef = ref(database, FirebasePath.SUPER_GAME);

  useEffect(() => {
    onValue(gameDataRef, (snapshot) => {
      const data = snapshot.val();
      setGameData(data);
    });

    onValue(superGameDataRef, (snapshot) => {
      const data = snapshot.val();
      setSuperGameData(data);
    });
  }, []);

  useEffect(() => {
    if (gameData?.currentRound) {
      const categoriesRef = ref(
        database,
        `${FirebasePath.QUESTIONS}/${gameData.currentRound}/${FirebasePath.CATEGORIES}`
      );
      onValue(categoriesRef, (snapshot) => {
        const data = snapshot.val();

        const names = Object.keys(data || {}).map((key) => data[key].name);
        setCategories(names);
      });
    }
  }, [gameData?.currentRound]);

  const screenKey = gameData?.activeGameScreen;
  const currentScreen = screenKey ? ActiveGameScreens[screenKey] : undefined;

  return {
    superGameData,
    currentScreen,
    categories,
  };
};

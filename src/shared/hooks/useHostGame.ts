import { onValue, ref, set, remove, child, push } from "firebase/database";
import { database } from "../services/firebase/firebase";
import { FirebasePath } from "../constants/firebase";
import { useEffect, useState } from "react";
import { GameDataType, QuestionType, UsersType } from "../types";
import { ActiveGameScreens, ROUTES } from "../constants/routes";

export const useHostGameControl = () => {
  const [rounds, setRounds] = useState<QuestionType>();
  const [gameData, setGameData] = useState<GameDataType>();
  const [activeUserId, setActiveUserId] = useState<string>();

  const [users, setUsers] = useState<UsersType>();

  const questionRef = ref(database, FirebasePath.QUESTIONS);
  const gameDataRef = ref(database, FirebasePath.GAME_DATA);
  const usersRef = ref(database, FirebasePath.USERS);

  const activeUserRef = ref(database, FirebasePath.ACTIVE_USER);

  const setUserName = (userId: string, name: string) => {
    const userRef = ref(database, `${FirebasePath.USERS}/${userId}/name`);
    set(userRef, name);
  };

  const setUserCount = (userId: string, count: number) => {
    const userRef = ref(database, `${FirebasePath.USERS}/${userId}/count`);
    set(userRef, count);
  };

  const setUserIsReady = (userId: string, isReady: boolean) => {
    const userRef = ref(database, `${FirebasePath.USERS}/${userId}/isReady`);
    set(userRef, isReady);
  };

  const deleteUser = (userId: string) => {
    const userRef = ref(database, `${FirebasePath.USERS}/${userId}`);
    remove(userRef);
  };

  const setCurrentRound = (round: string) => {
    const gameDataRef = ref(
      database,
      `${FirebasePath.GAME_DATA}/${FirebasePath.CURRENT_ROUND}`
    );
    set(gameDataRef, round);
  };

  const resetCurrentRound = () => {
    const gameDataRef = ref(
      database,
      `${FirebasePath.GAME_DATA}/${FirebasePath.CURRENT_ROUND}`
    );
    remove(gameDataRef);
  };

  const setCurrentQuestionData = (
    currentRound?: string,
    currentCategory?: string,
    currentPrice?: string
  ) => {
    if (!currentCategory || !currentRound || !currentPrice) return;

    set(gameDataRef, {
      currentCategory,
      currentPrice,
      currentRound,
    });
  };

  const checkIfCurrentQuestionOpen = (
    currentRound?: string,
    currentCategory?: string,
    currentPrice?: string
  ) => {
    if (!currentCategory || !currentRound || !currentPrice) return false;

    return (
      gameData?.currentCategory === currentCategory &&
      gameData?.currentPrice === currentPrice &&
      gameData?.currentRound === currentRound
    );
  };

  const isQuestionSelected = Boolean(
    gameData?.currentRound &&
      gameData?.currentCategory &&
      gameData?.currentPrice
  );

  const currentQuestion =
    !gameData?.currentRound ||
    !gameData?.currentCategory ||
    !gameData?.currentPrice
      ? undefined
      : rounds?.[gameData?.currentRound]?.categories?.[
          gameData?.currentCategory
        ]?.questions?.[gameData?.currentPrice];

  const currentQuestionData = {
    currentCategory:
      gameData?.currentRound &&
      rounds?.[gameData?.currentRound]?.categories?.[gameData?.currentCategory]
        ?.name,
    currentPrice: gameData?.currentPrice,
  };

  const resetCurrentQuestionData = () => {
    const currentPiceRef = child(gameDataRef, `${FirebasePath.CURRENT_PRICE}`);

    remove(currentPiceRef);
  };

  const resetActiveUser = () => {
    remove(activeUserRef);
  };

  const changeUserCount = (userId: string, changeCount: number) => {
    const newCount = (users?.[userId]?.count || 0) + changeCount;
    const userRef = child(usersRef, `${userId}/count`);

    set(userRef, newCount);
  };

  const setQuestionStatus = (
    isAnswered: boolean,
    currentRoundId?: string,
    currentCategoryId?: string,
    currentPrice?: string
  ) => {
    if (!currentRoundId || !currentCategoryId || !currentPrice) return;
    const isAnsweredRef = child(
      questionRef,
      `${currentRoundId}/${FirebasePath.CATEGORIES}/${currentCategoryId}/${FirebasePath.QUESTIONS}/${currentPrice}/isAnswered`
    );

    set(isAnsweredRef, isAnswered);
  };

  const setSharedUrl = (url: string) => {
    const sharedUrlRef = child(gameDataRef, `${FirebasePath.SHARED_URL}`);

    set(sharedUrlRef, url);
  };

  const setActiveGameScreen = (screen: keyof typeof ActiveGameScreens) => {
    const activeGameScreenRef = child(
      gameDataRef,
      `${FirebasePath.ACTIVE_GAME_SCREEN}`
    );

    set(activeGameScreenRef, screen);
  };

  const setQuestionShowStatus = (isShow: boolean) => {
    const gameDataRef = ref(
      database,
      `${FirebasePath.GAME_DATA}/${FirebasePath.IS_QUESTION_SHOW}`
    );
    set(gameDataRef, isShow);
  };

  const setQuestionAnswerBan = (isBan: boolean) => {
    const gameDataRef = ref(
      database,
      `${FirebasePath.GAME_DATA}/${FirebasePath.IS_ANSWER_BAN}`
    );
    set(gameDataRef, isBan);
  };

  const setCurrentUser = (userId: string) => {
    set(activeUserRef, userId);
  };

  const setIsDemoAvailable = (isAvailable: boolean) => {
    const gameDataRef = ref(
      database,
      `${FirebasePath.GAME_DATA}/${FirebasePath.IS_DEMO_AVAILABLE}`
    );
    set(gameDataRef, isAvailable);
  };

  const resetSpinTry = () => {
    const isFirstTimeSpinAvailableRef = child(
      gameDataRef,
      FirebasePath.IS_FIRST_TIME_SPIN_AVAILABLE
    );
    set(isFirstTimeSpinAvailableRef, true);
  };

  useEffect(() => {
    onValue(gameDataRef, (snapshot) => {
      const data = snapshot.val();

      setGameData(data || {});
    });

    onValue(usersRef, (snapshot) => {
      const data = snapshot.val();

      setUsers(data || {});
    });
  }, []);

  useEffect(() => {
    onValue(questionRef, (snapshot) => {
      const data = snapshot.val();

      setRounds(data || {});
    });

    onValue(activeUserRef, (snapshot) => {
      const data = snapshot.val();

      setActiveUserId(data);
    });
  }, []);

  return {
    activeUserId,
    currentQuestion,
    rounds,
    gameData,
    users,
    isQuestionSelected,
    currentQuestionData,
    setUserName,
    setUserCount,
    setUserIsReady,
    deleteUser,
    setCurrentRound,
    setCurrentQuestionData,
    resetCurrentQuestionData,
    checkIfCurrentQuestionOpen,
    resetActiveUser,
    changeUserCount,
    setQuestionStatus,
    setSharedUrl,
    resetCurrentRound,
    setActiveGameScreen,
    setQuestionShowStatus,
    setQuestionAnswerBan,
    setCurrentUser,
    setIsDemoAvailable,
    resetSpinTry,
  };
};

import { child, get, onValue, push, ref, remove, set } from "firebase/database";
import { database } from "../services/firebase/firebase";
import { useEffect, useState } from "react";
import { FirebasePath } from "../constants/firebase";
import { QuestionItemType, QuestionType, SuperGameType } from "../types";

const initialRound = {
  prices: [100, 200, 300, 400, 500],
};

export const useEditingQuestions = () => {
  const [rounds, setRounds] = useState<QuestionType>({});
  const [superGame, setSuperGame] = useState<SuperGameType>({});
  const questionsRef = ref(database, FirebasePath.QUESTIONS);
  const superGameRef = ref(database, FirebasePath.SUPER_GAME);

  const addNewRound = () => {
    push(questionsRef, initialRound);
  };

  const deleteRound = (id: string) => {
    const path = child(questionsRef, id);
    remove(path);
  };

  const setPrices = (roundId: string, prices: number[]) => {
    const path = child(questionsRef, `${roundId}/${FirebasePath.PRICES}`);
    set(path, prices);
  };

  const addCategory = (roundId: string, categoryName: string) => {
    const path = child(questionsRef, `${roundId}/${FirebasePath.CATEGORIES}`);

    const category = {
      name: categoryName,
      questions: [],
    };

    push(path, category);
  };

  const deleteCategory = (roundId: string, categoryId: string) => {
    const path = child(
      questionsRef,
      `${roundId}/${FirebasePath.CATEGORIES}/${categoryId}`
    );
    remove(path);
  };

  const editCategoryName = (
    roundId: string,
    categoryId: string,
    newName: string
  ) => {
    const path = child(
      questionsRef,
      `${roundId}/${FirebasePath.CATEGORIES}/${categoryId}/name`
    );

    set(path, newName);
  };

  const setQuestion = (
    roundId: string,
    categoryId: string,
    price: string,
    question: QuestionItemType
  ) => {
    const path = child(
      questionsRef,
      `${roundId}/${FirebasePath.CATEGORIES}/${categoryId}/${FirebasePath.QUESTIONS}/${price}`
    );

    set(path, question);
  };

  const deleteQuestion = (
    roundId: string,
    categoryId: string,
    price: string
  ) => {
    const path = child(
      questionsRef,
      `${roundId}/${FirebasePath.CATEGORIES}/${categoryId}/${FirebasePath.QUESTIONS}/${price}`
    );

    remove(path);
  };

  const setCatInABagStatus = (
    roundId: string,
    categoryId: string,
    price: string,
    status: boolean
  ) => {
    const path = child(
      questionsRef,
      `${roundId}/${FirebasePath.CATEGORIES}/${categoryId}/${FirebasePath.QUESTIONS}/${price}/${FirebasePath.IS_CAT_IN_A_BAG}`
    );

    set(path, status);
  };

  const setAuctionQuestionStatus = (
    roundId: string,
    categoryId: string,
    price: string,
    status: boolean
  ) => {
    const path = child(
      questionsRef,
      `${roundId}/${FirebasePath.CATEGORIES}/${categoryId}/${FirebasePath.QUESTIONS}/${price}/${FirebasePath.IS_AUCTION_QUESTION}`
    );

    set(path, status);
  };

  const setSuperGameTheme = (question: string) => {
    const path = child(superGameRef, `${FirebasePath.THEME}`);

    set(path, question);
  };

  const setSuperGameQuestion = (question: string) => {
    const path = child(superGameRef, `${FirebasePath.QUESTION}`);

    set(path, question);
  };

  const setSuperGameAnswer = (answer: string) => {
    const path = child(superGameRef, `${FirebasePath.ANSWER}`);

    set(path, answer);
  };

  useEffect(() => {
    onValue(questionsRef, (snapshot) => {
      const data = snapshot.val();

      setRounds(data || {});
    });

    onValue(superGameRef, (snapshot) => {
      const data = snapshot.val();

      setSuperGame(data || {});
    });
  }, []);

  return {
    rounds,
    superGame,
    addNewRound,
    deleteRound,
    setPrices,
    addCategory,
    deleteCategory,
    setQuestion,
    editCategoryName,
    deleteQuestion,
    setCatInABagStatus,
    setAuctionQuestionStatus,
    setSuperGameQuestion,
    setSuperGameAnswer,
    setSuperGameTheme,
  };
};

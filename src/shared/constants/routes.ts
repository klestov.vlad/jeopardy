export enum ActiveGameScreens {
  PREVIEW_SCREEN = "/preview",
  SHARED_GAME_SCREEN = "/shared-game",
  USERS_GAME_SCORE = "/users-game-score",
  QR_CODE_SCREEN = "/qr-code",

  PREVIEW_SUPER_GAME_SCREEN = "/preview-super-game",
  SUPER_GAME_SCREEN = "/super-game",
  FINAL_GAME_SCORE = "/final-game-score",
  SUPER_GAME_ANSWERS = "/super-game-answers",
  SPIN_TO_WIN_SCREEN = "/spin-to-win",
}

export enum ControlScreens {
  QUESTION_SCREEN = "/question",
  GAME_HOST_SCREEN = "/game-host",
}

export enum GameScreens {
  HOME = "/",
  INTRODUCE_SCREEN = "/introduce",
  GAME_SCREEN = "/game",
  COMING_SOON_SCREEN = "/coming-soon",
  READY_TO_PLAY_SCREEN = "/ready-to-play",
}

export const ROUTES = {
  ...GameScreens,
  ...ControlScreens,
  ...ActiveGameScreens,
};
